package com.mojang.authlib.yggdrasil;

import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.ProfileLookupCallback;
import lombok.val;
import lombok.var;
import ru.cristalix.launcher.LaunchServerApiInstance;

import java.util.Arrays;

public final class YggdrasilGameProfileRepository implements GameProfileRepository {

    private static final int MAX_BATCH_SIZE = 100;

    public YggdrasilGameProfileRepository() {
    }

    @Override
    public void findProfilesByNames(String[] usernames, Agent agent, ProfileLookupCallback callback) {
        var offset = 0;
        while (offset < usernames.length) {
            val sliceUsernames = Arrays.copyOfRange(usernames, offset, Math.min(offset + MAX_BATCH_SIZE, usernames.length));
            offset += MAX_BATCH_SIZE;

            LaunchServerApiInstance.API.getProfilesByUsernames(sliceUsernames).thenAccept(response -> {
                for (val profile : response.getProfiles()) {
                    callback.onProfileLookupSucceeded(YggdrasilMinecraftSessionService.toGameProfile(profile));
                }
            }).exceptionally(t -> {
                if (!(t instanceof Exception)) {
                    t.printStackTrace();
                    return null;
                }
                for (val username : sliceUsernames) {
                    callback.onProfileLookupFailed(new GameProfile(null, username), (Exception) t);
                }
                return null;
            });
        }
    }
}
