package com.mojang.authlib.yggdrasil;

import com.google.common.collect.Iterables;
import com.mojang.authlib.AuthenticationService;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.minecraft.BaseMinecraftSessionService;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftProfileTexture.Type;
import com.mojang.authlib.properties.Property;
import lombok.val;
import ru.cristalix.launcher.LaunchServerApiInstance;
import ru.cristalix.launcher.api.data.ProfileData;

import java.net.InetAddress;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class YggdrasilMinecraftSessionService extends BaseMinecraftSessionService {

    public static final String SKIN_URL_PROPERTY = "skinURL";
    public static final String SKIN_DIGEST_PROPERTY = "skinDigest";
    public static final String CAPE_URL_PROPERTY = "capeURL";
    public static final String CAPE_DIGEST_PROPERTY = "capeDigest";

    public YggdrasilMinecraftSessionService(AuthenticationService service) {
        super(service);
    }

    public static void fillTextureProperties(GameProfile gameProfile, ProfileData profile) {
        val properties = gameProfile.getProperties();
        val skin = profile.getSkin();
        if (skin != null) {
            properties.put(SKIN_URL_PROPERTY,
                    new Property(SKIN_URL_PROPERTY, skin.getUrl(), ""));
            properties.put(SKIN_DIGEST_PROPERTY,
                    new Property(SKIN_DIGEST_PROPERTY,skin.getSha256(), ""));
        }
        val cape = profile.getCape();
        if (cape != null) {
            properties.put(CAPE_URL_PROPERTY,
                    new Property(CAPE_URL_PROPERTY, cape.getUrl(), ""));
            properties.put(CAPE_DIGEST_PROPERTY,
                    new Property(CAPE_DIGEST_PROPERTY, cape.getSha256(), ""));
        }
    }

    public static GameProfile toGameProfile(ProfileData profileData) {
        val profile = new GameProfile(profileData.getUuid(), profileData.getUsername());
        fillTextureProperties(profile, profileData);
        return profile;
    }

    @Override
    public GameProfile fillProfileProperties(GameProfile gameProfile, boolean requireSecure) {
        val uuid = gameProfile.getId();
        if (uuid == null) {
            return gameProfile;
        }
        try {
            val profile = LaunchServerApiInstance.API.getProfileByUuid(uuid)
                    .get(5000, TimeUnit.MILLISECONDS).getProfile();
            return toGameProfile(profile);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Map<Type, MinecraftProfileTexture> getTextures(GameProfile profile, boolean requireSecure) {
        val textures = new EnumMap<Type, MinecraftProfileTexture>(Type.class);
        val skinURL = Iterables.getFirst(profile.getProperties().get(SKIN_URL_PROPERTY), null);
        val skinDigest = Iterables.getFirst(profile.getProperties().get(SKIN_DIGEST_PROPERTY), null);
        if (skinURL != null && skinDigest != null) {
            textures.put(Type.SKIN, new MinecraftProfileTexture(skinURL.getValue(), skinDigest.getValue()));
        }
        val cloakURL = Iterables.getFirst(profile.getProperties().get(CAPE_URL_PROPERTY), null);
        val cloakDigest = Iterables.getFirst(profile.getProperties().get(CAPE_DIGEST_PROPERTY), null);
        if (cloakURL != null && cloakDigest != null) {
            textures.put(Type.CAPE, new MinecraftProfileTexture(cloakURL.getValue(), cloakDigest.getValue()));
        }
        return textures;
    }

    public GameProfile hasJoinedServer(GameProfile gameProfile, String serverId) {
        try {
            val profile = LaunchServerApiInstance.API.hasJoined(gameProfile.getName(), serverId)
                    .get(5000, TimeUnit.MILLISECONDS).getProfile();
            return toGameProfile(profile);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public GameProfile hasJoinedServer(GameProfile profile, String serverID, InetAddress address) {
        return hasJoinedServer(profile, serverID);
    }

    @Override
    public void joinServer(GameProfile profile, String accessToken, String serverId) throws AuthenticationException {
        try {
            LaunchServerApiInstance.API.joinServer(accessToken, serverId).get(5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            throw new AuthenticationException(e.getMessage());
        }
    }
}
