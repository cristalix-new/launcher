package com.mojang.authlib.minecraft;

import lombok.var;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

public final class MinecraftProfileTexture {

    public static final Set<Type> PROFILE_TEXTURE_TYPES = Collections.unmodifiableSet(EnumSet.allOf(Type.class));
    public static final int PROFILE_TEXTURE_COUNT = PROFILE_TEXTURE_TYPES.size();
    private final String url;
    private final String hash;
    private Map<String, String> metadata;

    public MinecraftProfileTexture(String url, final Map<String, String> metadata) {
        this(url);
        this.metadata = metadata;
    }

    public MinecraftProfileTexture(String url) {
        this(url, baseName(url));
    }

    public MinecraftProfileTexture(String url, String hash) {
        this.url = url;
        this.hash = hash;
    }

    private static String baseName(String url) {
        var name = url.substring(url.lastIndexOf('/') + 1);
        int extensionIndex = name.lastIndexOf('.');
        if (extensionIndex >= 0) {
            name = name.substring(0, extensionIndex);
        }
        return name;
    }

    @Override
    public String toString() {
        return String.format("MinecraftProfileTexture{url='%s',hash=%s}", url, hash);
    }

    @SuppressWarnings("unused")
    public String getHash() {
        return hash;
    }

    @SuppressWarnings({"unused", "SameReturnValue"})
    public String getMetadata(String key) {
        Map<String, String> metadata = this.metadata;
        return metadata == null ? null : metadata.get(key);
    }

    @SuppressWarnings("unused")
    public String getUrl() {
        return url;
    }

    public enum Type {
        SKIN, CAPE, ELYTRA
    }
}