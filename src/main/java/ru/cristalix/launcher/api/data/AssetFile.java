package ru.cristalix.launcher.api.data;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class AssetFile {
    @SerializedName("downloadUrl")
    String downloadUrl;
    @SerializedName("path")
    String path;
    @SerializedName("sha256")
    String sha256;
    @SerializedName("size")
    long size;
}