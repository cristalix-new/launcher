package ru.cristalix.launcher.api.data;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class SessionData {
    @SerializedName("accessToken")
    String accessToken;
    @SerializedName("profile")
    ProfileData profile;
}
