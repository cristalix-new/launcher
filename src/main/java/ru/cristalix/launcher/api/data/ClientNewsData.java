package ru.cristalix.launcher.api.data;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class ClientNewsData {
    @SerializedName("imageUrl")
    String imageUrl;
    @SerializedName("text")
    String text;
    @SerializedName("title")
    String title;
}
