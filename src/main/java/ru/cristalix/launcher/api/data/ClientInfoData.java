package ru.cristalix.launcher.api.data;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class ClientInfoData {
    @SerializedName("id")
    String id;
    @SerializedName("imageId")
    String imageId;
    @SerializedName("title")
    String title;
    @SerializedName("online")
    boolean online;
    @SerializedName("currentPlayers")
    int currentPlayers;
    @SerializedName("maxPlayers")
    int maxPlayers;
    @SerializedName("information")
    String information;
    @SerializedName("technical")
    boolean technical;
    @SerializedName("lastNews")
    ClientNewsData lastNews;
}
