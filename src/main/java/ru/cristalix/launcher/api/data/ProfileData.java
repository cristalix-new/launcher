package ru.cristalix.launcher.api.data;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class ProfileData {
    @SerializedName("uuid")
    UUID uuid;
    @SerializedName("username")
    String username;
    @SerializedName("skin")
    ProfileImage skin;
    @SerializedName("cape")
    ProfileImage cape;
}