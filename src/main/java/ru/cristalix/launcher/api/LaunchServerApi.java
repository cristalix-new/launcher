package ru.cristalix.launcher.api;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import ru.cristalix.launcher.api.data.*;
import ru.cristalix.launcher.utils.GsonInstance;
import ru.cristalix.launcher.utils.IOUtil;
import ru.cristalix.launcher.utils.LauncherThreads;
import ru.cristalix.launcher.utils.SneakyThrow;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public final class LaunchServerApi {
    private static final String AUTH_ENDPOINT = "/auth";
    private static final String LAUNCHER_INFO_ENDPOINT = "/launcher";
    private static final String LAUNCHER_ASSETS_ENDPOINT = "/launcherAssets";
    private static final String TOKEN_REFRESH_ENDPOINT = "/token";
    private static final String NEWS_ENDPOINT = "/news";
    private static final String CLIENTS_ENDPOINT = "/clients";
    private static final String CLIENT_BY_ID_ENDPOINT = "/client";

    private static final String JOIN_SERVER_ENDPOINT = "/minecraft/joinServer";
    private static final String HAS_JOINED_ENDPOINT = "/minecraft/hasJoined";
    private static final String PROFILE_BY_USERNAME_ENDPOINT = "/minecraft/profile/username";
    private static final String PROFILE_BY_UUID_ENDPOINT = "/minecraft/profile/uuid";
    private static final String PROFILES_BY_USERNAMES_ENDPOINT = "/minecraft/profile/usernames";

    private final String serverUrl;

    public LaunchServerApi(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class LaunchServerApiResponse<T> {
        @SerializedName("success")
        boolean success;
        @SerializedName("data")
        T data;
        @SerializedName("error")
        String error;
    }

    private <T> CompletableFuture<T> request(String endPoint, Object data, Class<T> responseClass) {
        return LauncherThreads.supply(() -> {
            val url = serverUrl + endPoint;

            try {
                HttpURLConnection request;

                val gson = GsonInstance.GSON;
                if (data == null) {
                    request = IOUtil.get(url);
                } else {
                    request = IOUtil.post(url, gson.toJson(data).getBytes(StandardCharsets.UTF_8), "application/json");
                }

                val code = request.getResponseCode();
                try (val reader = IOUtil.reader(code == 200 ? request.getInputStream()
                        : request.getErrorStream(), StandardCharsets.UTF_8)) {
                    val response = gson.<LaunchServerApiResponse<T>>fromJson(reader,
                            new ParameterizedType() {
                                @Override
                                public Type[] getActualTypeArguments() {
                                    return new Type[]{responseClass};
                                }

                                @Override
                                public Type getRawType() {
                                    return LaunchServerApiResponse.class;
                                }

                                @Override
                                public Type getOwnerType() {
                                    return null;
                                }
                            });

                    if (!response.success) {
                        throw new LaunchServerApiException(response.error);
                    }

                    return response.data;
                }
            } catch (Exception ex) {
                throw SneakyThrow.sneaky(ex);
            }
        });
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class AuthResponse {
        @SerializedName("refreshToken")
        String refreshToken;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class AuthRequest {
        @SerializedName("username")
        String username;
        @SerializedName("password")
        String password;
        @SerializedName("otpCode")
        String otpCode;
    }

    public CompletableFuture<AuthResponse> auth(String username, String password, String otpCode) {
        return request(AUTH_ENDPOINT, new AuthRequest(username, password, otpCode), AuthResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class LauncherInfoResponse {
        @SerializedName("downloadUrl")
        String downloadUrl;
        @SerializedName("hash")
        String hash;
        @SerializedName("size")
        long size;
        @SerializedName("assetsUrl")
        String assetsUrl;
        @SerializedName("assetsHash")
        String assetsHash;
    }

    public CompletableFuture<LauncherInfoResponse> getLauncherInfo() {
        return request(LAUNCHER_INFO_ENDPOINT, null, LauncherInfoResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class TokenRefreshResponse {
        @SerializedName("session")
        SessionData session;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class TokenRefreshRequest {
        @SerializedName("refreshToken")
        String refreshToken;
    }

    public CompletableFuture<TokenRefreshResponse> refreshAccessToken(String refreshToken) {
        return request(TOKEN_REFRESH_ENDPOINT, new TokenRefreshRequest(refreshToken), TokenRefreshResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class NewsResponse {
        @SerializedName("news")
        NewsItem[] news;
    }

    public CompletableFuture<NewsResponse> getNews() {
        return request(NEWS_ENDPOINT, null, NewsResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class ClientsResponse {
        @SerializedName("clients")
        ClientInfoData[] clients;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class ClientsRequest {
        @SerializedName("accessToken")
        String accessToken;
    }

    public CompletableFuture<ClientsResponse> getClients(String accessToken) {
        return request(CLIENTS_ENDPOINT, new ClientsRequest(accessToken), ClientsResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class ClientByIdResponse {
        @SerializedName("assetFiles")
        AssetFile[] assetFiles;
        @SerializedName("clientFiles")
        ClientFile[] clientFiles;
        @SerializedName("verifiedFiles")
        String[] verifiedFiles;
        @SerializedName("classPathFiles")
        String[] classPathFiles;
        @SerializedName("version")
        String version;
        @SerializedName("jvmArgs")
        List<String> jvmArgs;
        @SerializedName("clientArgs")
        List<String> clientArgs;
        @SerializedName("mainClass")
        String mainClass;
        @SerializedName("autoLoginHostName")
        String autoLoginHostName;
        @SerializedName("autoLoginPort")
        int autoLoginPort;
        @SerializedName("supportedSignals")
        List<String> supportedSignals;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class ClientByIdRequest {
        @SerializedName("accessToken")
        String accessToken;
        @SerializedName("clientId")
        String clientId;
    }

    public CompletableFuture<ClientByIdResponse> getClient(String accessToken, String clientId) {
        return request(CLIENT_BY_ID_ENDPOINT, new ClientByIdRequest(accessToken, clientId), ClientByIdResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class HasJoinedResponse {
        @SerializedName("profile")
        ProfileData profile;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class HasJoinedRequest {
        @SerializedName("username")
        String username;
        @SerializedName("serverId")
        String serverId;
    }

    public CompletableFuture<HasJoinedResponse> hasJoined(String username, String serverId) {
        return request(HAS_JOINED_ENDPOINT, new HasJoinedRequest(username, serverId), HasJoinedResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class JoinServerRequest {
        @SerializedName("accessToken")
        String accessToken;
        @SerializedName("serverId")
        String serverId;
    }

    public CompletableFuture<Void> joinServer(String accessToken, String serverId) {
        return request(JOIN_SERVER_ENDPOINT, new JoinServerRequest(accessToken, serverId), Void.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class ProfileByUsernameResponse {
        @SerializedName("profile")
        ProfileData profile;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class ProfileByUsernameRequest {
        @SerializedName("username")
        String username;
    }

    public CompletableFuture<ProfileByUsernameResponse> getProfileByUsername(String username) {
        return request(PROFILE_BY_USERNAME_ENDPOINT, new ProfileByUsernameRequest(username), ProfileByUsernameResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class ProfileByUuidResponse {
        @SerializedName("profile")
        ProfileData profile;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class ProfileByUuidRequest {
        @SerializedName("uuid")
        UUID uuid;
    }

    public CompletableFuture<ProfileByUuidResponse> getProfileByUuid(UUID uuid) {
        return request(PROFILE_BY_UUID_ENDPOINT, new ProfileByUuidRequest(uuid), ProfileByUuidResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class ProfilesByUsernamesResponse {
        @SerializedName("profiles")
        ProfileData[] profiles;
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class ProfilesByUsernamesRequest {
        @SerializedName("usernames")
        String[] usernames;
    }

    public CompletableFuture<ProfilesByUsernamesResponse> getProfilesByUsernames(String[] usernames) {
        return request(PROFILES_BY_USERNAMES_ENDPOINT, new ProfilesByUsernamesRequest(usernames),
                ProfilesByUsernamesResponse.class);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class LauncherAssetsResponse {
        @SerializedName("url")
        String url;
        @SerializedName("hash")
        String hash;
    }

    public CompletableFuture<LauncherAssetsResponse> getAssets() {
        return request(LAUNCHER_ASSETS_ENDPOINT, null, LauncherAssetsResponse.class);
    }
}