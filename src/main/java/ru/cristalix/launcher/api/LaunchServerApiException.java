package ru.cristalix.launcher.api;

public class LaunchServerApiException extends RuntimeException {
    public static final String WRONG_TOKEN = "wrong-token";
    public static final String OTP_REQUIRED = "otp-required";
    public static final String WRONG_USERNAME_OR_PASSWORD = "wrong-username-or-password";
    public static final String CLIENT_NOT_FOUND = "client-not-found";
    public static final String FAILED_TO_AUTH = "failed-to-auth";
    public static final String PLAYER_NOT_FOUND = "player-not-found";
    public static final String PROFILE_NOT_FOUND = "profile-not-found";
    public static final String WRONG_OTP_CODE = "wrong-otp-code";
    public static final String INTERNAL_SERVER_ERROR = "internal-server-error";

    public LaunchServerApiException(String message) {
        super(message);
    }
}