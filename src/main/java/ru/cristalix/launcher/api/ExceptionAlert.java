package ru.cristalix.launcher.api;

import com.sun.javafx.application.PlatformImpl;
import dev.xdark.asminline.AsmBlock;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import lombok.val;
import ru.cristalix.launcher.Launcher;
import ru.cristalix.launcher.gui.ToolKit;
import ru.cristalix.launcher.gui.controllers.MainController;
import ru.cristalix.launcher.utils.DocumentUtil;
import ru.cristalix.launcher.utils.UnsafeUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class ExceptionAlert extends Alert {

    private static final byte[] IMAGE = AsmBlock.inlineA(b -> {
        try {
            b.array(Files.readAllBytes(Paths.get(System.getProperty("gradle.projectDir"), "error.png")));
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    });

    private ExceptionAlert(Throwable t, String msg) {
        super(AlertType.ERROR);
        setTitle("Ошибка");
        setContentText(msg);
        val grid = new GridPane();
        grid.setMaxWidth(Double.MAX_VALUE);
        if (t != null) {
            String header = MainController.translateThrowable(t);
            setHeaderText(header);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            String exText = sw.toString();
            Label lbl = new Label("Стек трейс ошибки:");
            TextArea txt = new TextArea(exText);
            txt.setEditable(false);
            txt.setWrapText(false);
            txt.setMaxWidth(Double.MAX_VALUE);
            txt.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(txt, Priority.ALWAYS);
            GridPane.setHgrow(txt, Priority.ALWAYS);
            grid.add(lbl, 0, 0);
            grid.add(txt, 0, 1);
        }
        Hyperlink link = new Hyperlink("Сообщить о баге");
        link.setOnAction(e -> {
            try {
                DocumentUtil.showDocument("https://vk.me/cristalixreloaded");
            } catch (IOException ex) {
                show(ex, "Открытие ссылки");
            }
        });
        grid.add(link, 0, 2);
        val dialogPane = getDialogPane();
        dialogPane.setExpandableContent(grid);
        dialogPane.setExpanded(true);
        // Set icons
        {
            val stream = Launcher.class.getResourceAsStream("/images/icons/icon.png");
            if (stream != null) {
                Stage stage = (Stage) getDialogPane().getScene().getWindow();
                stage.getIcons().add(new Image(stream));
            }
        }
        setGraphic(new ImageView(new Image(new ByteArrayInputStream(IMAGE))));
        setOnCloseRequest(__ -> Runtime.getRuntime().halt(1));
    }

    public static void show(Throwable t) {
        show(t, null);
    }

    public static void show(Throwable t, String msg) {
        UnsafeUtil.unsafe().ensureClassInitialized(ToolKit.class);
        PlatformImpl.startup(() -> {
            new ExceptionAlert(t, msg).show();
        });
    }
}