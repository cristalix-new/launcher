package ru.cristalix.launcher.gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.util.Callback;
import lombok.SneakyThrows;
import lombok.val;
import ru.cristalix.launcher.gui.controllers.MainController;
import ru.cristalix.launcher.gui.controllers.elements.BackgroundButtonController;
import ru.cristalix.launcher.gui.controllers.elements.NewsController;
import ru.cristalix.launcher.gui.controllers.elements.ServerController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class ToolKit {

  public static final FXMLLoader LOADER = createLoader();

  private ToolKit() { }

  @SneakyThrows(IllegalAccessException.class)
  public static void cacheController(Object controller) {
    for (val field : controller.getClass().getDeclaredFields()) {
      if (field.isAnnotationPresent(FXML.class)) {
        field.setAccessible(true);
        val o = field.get(controller);
        if (o instanceof Node) {
          val node = (Node) o;
          node.setCache(true);
          node.setCacheHint(CacheHint.SPEED);
        }
      }
    }
  }

  public static <T> T load(FXMLLoader loader) throws IOException {
    val loaded = (T) loader.load();
    if (loaded instanceof Node) {
      val node = (Node) loaded;
      node.setCache(true);
      node.setCacheHint(CacheHint.SPEED);
    }
    return loaded;
  }

  public static FXMLLoader createLoader() {
    val loader = new FXMLLoader();
    loader.setCharset(StandardCharsets.UTF_8);
    loader.setClassLoader(ToolKit.class.getClassLoader());
    loader.setControllerFactory(new Callback<Class<?>, Object>() {
      @Override
      public Object call(Class<?> aClass) {
        if (aClass == MainController.class) {
          return new MainController();
        }
        if (aClass == NewsController.class) {
          return new NewsController();
        }
        if (aClass == ServerController.class) {
          return new ServerController();
        }
        if (aClass == BackgroundButtonController.class) {
          return new BackgroundButtonController();
        }
        throw new UnsupportedOperationException(aClass.getName());
      }
    });
    return loader;
  }
}
