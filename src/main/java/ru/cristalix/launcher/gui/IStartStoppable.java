package ru.cristalix.launcher.gui;

import javafx.animation.Animation;
import lombok.RequiredArgsConstructor;

public interface IStartStoppable {
    void start();

    void stop();

    @RequiredArgsConstructor
    class AnimationStartStoppable implements IStartStoppable {
        private final Animation animation;

        @Override
        public void start() {
            animation.playFromStart();
        }

        @Override
        public void stop() {
            animation.stop();
        }
    }
}
