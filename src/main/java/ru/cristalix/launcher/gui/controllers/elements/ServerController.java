package ru.cristalix.launcher.gui.controllers.elements;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ru.cristalix.launcher.utils.TextFormat;

import java.net.URL;
import java.util.ResourceBundle;

public class ServerController implements Initializable {

    @FXML
    private ImageView backgroundImageView;
    @FXML
    private ImageView iconImageView;
    @FXML
    private Label titleLabel;
    @FXML
    private Label statusLabel;
    @FXML
    private ToggleButton starButton;

    private final StringProperty imageId = new SimpleStringProperty("minigames");

    public String getImageId() {
        return imageId.get();
    }

    public StringProperty imageIdProperty() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId.set(imageId);
    }

    private final StringProperty title = new SimpleStringProperty("MiniGames");

    public String getTitle() {
        return title.get();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    private final IntegerProperty currentOnline = new SimpleIntegerProperty();

    public int getCurrentOnline() {
        return currentOnline.get();
    }

    public IntegerProperty currentOnlineProperty() {
        return currentOnline;
    }

    public void setCurrentOnline(int currentOnline) {
        this.currentOnline.set(currentOnline);
    }

    private final IntegerProperty maxOnline = new SimpleIntegerProperty();

    public int getMaxOnline() {
        return maxOnline.get();
    }

    public IntegerProperty maxOnlineProperty() {
        return maxOnline;
    }

    public void setMaxOnline(int maxOnline) {
        this.maxOnline.set(maxOnline);
    }

    private final BooleanProperty online = new SimpleBooleanProperty();

    public boolean isOnline() {
        return online.get();
    }

    public BooleanProperty onlineProperty() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online.set(online);
    }

    private final BooleanProperty selected = new SimpleBooleanProperty();

    public boolean isSelected() {
        return selected.get();
    }

    public BooleanProperty selectedProperty() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }

    private final BooleanProperty favourite = new SimpleBooleanProperty();

    public boolean isFavourite() {
        return favourite.get();
    }

    public BooleanProperty favouriteProperty() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite.set(favourite);
    }

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        backgroundImageView.imageProperty().bind(Bindings.createObjectBinding(() -> new Image(getClass().
                        getResourceAsStream(TextFormat.serverBackground(imageId.get()))),
                imageId));
        iconImageView.imageProperty().bind(Bindings.createObjectBinding(() -> new Image(getClass().
                        getResourceAsStream(TextFormat.serverIcon(imageId.get()))),
                imageId));
        titleLabel.textProperty().bind(title);
        statusLabel.textProperty().bind(Bindings.when(online)
                .then(Bindings.format("%d/%d", currentOnline, maxOnline))
                .otherwise("Выключен"));
        backgroundImageView.effectProperty().bind(Bindings.when(selected).then(new ColorAdjust())
                .otherwise(new ColorAdjust(0, -1, -0.4, 0)));
        favourite.bindBidirectional(starButton.selectedProperty());
    }
}
