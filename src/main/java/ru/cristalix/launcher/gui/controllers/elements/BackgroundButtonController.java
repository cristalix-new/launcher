package ru.cristalix.launcher.gui.controllers.elements;

import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ru.cristalix.launcher.utils.TextFormat;

import java.net.URL;
import java.util.ResourceBundle;

public class BackgroundButtonController implements Initializable {

    @FXML
    private ImageView backgroundImageView;

    private final IntegerProperty backgroundIndex = new SimpleIntegerProperty();

    public int getBackgroundIndex() {
        return backgroundIndex.get();
    }

    public IntegerProperty backgroundIndexProperty() {
        return backgroundIndex;
    }

    public void setBackgroundIndex(int backgroundIndex) {
        this.backgroundIndex.set(backgroundIndex);
    }

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        backgroundImageView.imageProperty().bind(Bindings.createObjectBinding(() -> new Image(getClass().
                getResourceAsStream(TextFormat.backgroundPreviewPath(backgroundIndex.get()))),
                backgroundIndex));
    }
}
