package ru.cristalix.launcher.gui.controllers;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.Setter;
import lombok.val;
import lombok.var;
import ru.cristalix.launcher.Launcher;
import ru.cristalix.launcher.api.ExceptionAlert;
import ru.cristalix.launcher.api.LaunchServerApiException;
import ru.cristalix.launcher.api.data.ClientFile;
import ru.cristalix.launcher.api.data.ClientInfoData;
import ru.cristalix.launcher.api.data.NewsItem;
import ru.cristalix.launcher.api.data.SessionData;
import ru.cristalix.launcher.download.AssetDownloader;
import ru.cristalix.launcher.download.Downloader;
import ru.cristalix.launcher.download.FileListVerifier;
import ru.cristalix.launcher.gui.IStartStoppable;
import ru.cristalix.launcher.gui.SmoothScroll;
import ru.cristalix.launcher.gui.ToolKit;
import ru.cristalix.launcher.gui.controllers.elements.BackgroundButtonController;
import ru.cristalix.launcher.gui.controllers.elements.NewsController;
import ru.cristalix.launcher.gui.controllers.elements.ServerController;
import ru.cristalix.launcher.relaunch.Relauncher;
import ru.cristalix.launcher.starter.ClientLauncher;
import ru.cristalix.launcher.starter.LauncherSession;
import ru.cristalix.launcher.utils.*;

import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainController implements Initializable {

    private static final int BACKGROUNDS = 7;

    @Setter private Launcher launcher;
    @Setter private Stage stage;

    @FXML
    private Button minimizeButton;
    @FXML
    private Button closeButton;
    @FXML
    private ImageView backgroundImage;
    @FXML
    private Pane mainPane;
    @FXML
    private ScrollBar newsScrollBar;
    @FXML
    private FlowPane newsFlowPane;
    @FXML
    private ScrollPane newsScrollPane;
    @FXML
    private ScrollBar serversScrollBar;
    @FXML
    private ScrollPane serversScrollPane;
    @FXML
    private FlowPane serversFlowPane;
    @FXML
    private Button homeButton;
    @FXML
    private Button newsButton;
    @FXML
    private Pane homePane;
    @FXML
    private Pane loginPane;
    @FXML
    private Pane loadingPane;
    @FXML
    private Button settingsButton;
    @FXML
    private Pane settingsPane;
    @FXML
    private Pane cristalixDiamond2;
    @FXML
    private Pane cristalixDiamond3;
    @FXML
    private Pane cristalixDiamond4;
    @FXML
    private HBox backgroundSettingButtonsHBox;
    @FXML
    private ToggleButton showFavouriteOnlyButton;
    @FXML
    private Button scrollServersPaneStateButton;
    @FXML
    private Button gridServersPaneStateButton;
    @FXML
    private Pane informationPane;
    @FXML
    private Pane serverNewsPane;
    @FXML
    private Button playButton;
    @FXML
    private Pane serversPaneBackground;
    @FXML
    private Button loginButton;
    @FXML
    private Pane mainNewsPanes;
    @FXML
    public Pane loggedPane;
    @FXML
    private TextField usernameOrEmailTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private ToggleButton showTechnicalServersButton;
    @FXML
    private ImageView serverNewsImageView;
    @FXML
    private Label serverNewsTitleLabel;
    @FXML
    private Label serverNewsTextLabel;
    @FXML
    private Label serverInfoLabel;
    @FXML
    private ScrollPane serverInfoScrollPane;
    @FXML
    private ScrollBar serverInfoScrollBar;
    @FXML
    private FlowPane serverInfoFlowPane;
    @FXML
    private Label loadingBigLabel;
    @FXML
    private Label loadingSmallLabel;
    @FXML
    private Button wipeClientButton;
    @FXML
    private Button logoutButton;
    @FXML
    private Button openGameDirectoryButton;
    @FXML
    private Button setGameDirectoryButton;
    @FXML
    private TextField otpField;
    @FXML
    private Button loginWithOtpButton;
    @FXML
    private Pane loginOtpPanes;
    @FXML
    private Pane loginInputPane;
    @FXML
    private Pane loginErrorPane;
    @FXML
    private Label loginErrorLabel;
    @FXML
    private Pane loginOtpErrorPane;
    @FXML
    private Label loginOtpErrorLabel;
    @FXML
    private Label loadingErrorLabel;
    @FXML
    private Slider memorySlider;
    @FXML
    private Label memoryForGameLabel;
    @FXML
    private CheckBox minimalGraphicSettingsCheckBox;
    @FXML
    private CheckBox autoMemoryCheckBox;
    @FXML
    private CheckBox disableDiscordRPCCheckBox;
    @FXML
    private CheckBox clientFullscreenCheckBox;
    @FXML
    private CheckBox autoEnterCheckBox;
    @FXML
    private ImageView logoImage;

    private final IntegerProperty currentBackgroundIndex = new SimpleIntegerProperty();
    private final IntegerProperty currentServerIndex = new SimpleIntegerProperty();

    private double xOffset;
    private double yOffset;

    private final BooleanProperty loggedIn = new SimpleBooleanProperty();
    private final BooleanProperty settingsOpened = new SimpleBooleanProperty();
    private final BooleanProperty loadingOpened = new SimpleBooleanProperty();
    private final BooleanProperty loginErrorShown = new SimpleBooleanProperty();
    private final BooleanProperty loginOtpErrorShown = new SimpleBooleanProperty();

    private final BooleanProperty isOtpOpened = new SimpleBooleanProperty();

    private final LongProperty selectedMemoryValue = new SimpleLongProperty();

    private final ObjectProperty<NewsItem[]> newsItems = new SimpleObjectProperty<>(new NewsItem[0]);
    private final ObjectProperty<ClientInfoData[]> clients = new SimpleObjectProperty<>(new ClientInfoData[0]);
    private final ObservableList<Image> cachedNewsImages = FXCollections.observableList(new ArrayList<>());

    private static final class NameMatcherHolder {

        static final Matcher matcher = Pattern.compile("([a-zA-Z0-9_.\\-]{0,16}|[а-яА-Я0-9_.\\-]{0,16})").matcher("");
    }

    private Animation diamondAnimation;

    private SessionData session;

    private void setLoginError(String text) {
        if (text != null) {
            loginErrorLabel.setText(text);
        }
        loginErrorShown.set(text != null);
    }

    private void setLoginOtpError(String text) {
        if (text != null) {
            loginOtpErrorLabel.setText(text);
        }
        loginOtpErrorShown.set(text != null);
    }

    private void setLoadingError(String text) {
        loadingErrorLabel.setVisible(true);
        loadingBigLabel.setVisible(false);
        loadingSmallLabel.setText(text);
    }

    private void setLoadingStatus(String bigLabelText, String smallLabelText) {
        if (bigLabelText != null) {
            loadingErrorLabel.setVisible(false);
            loadingBigLabel.setVisible(true);
            loadingBigLabel.setText(bigLabelText);
        }
        if (smallLabelText != null) {
            loadingSmallLabel.setText(smallLabelText);
        }
    }

    public static String translateThrowable(Throwable throwable) {
        val launchServerApiException = getBaseLaunchServerApiError(throwable);
        if (launchServerApiException == null) {
            val baseException = getBaseCause(throwable);
            if (baseException instanceof ConnectException) {
                return "Ошибка подключения к серверу";
            }
            val message = baseException.getLocalizedMessage();
            return message == null ? throwable.getMessage() : message;
        }

        switch (launchServerApiException.getMessage()) {
            case LaunchServerApiException.WRONG_USERNAME_OR_PASSWORD:
                return "Неправильный логин или пароль";
            case LaunchServerApiException.INTERNAL_SERVER_ERROR:
                return "Внутренняя ошибка сервера";
            case LaunchServerApiException.WRONG_OTP_CODE:
                return "Неправильный код";
        }

        return "Неизвестная ошибка";
    }

    private CompletableFuture<Void> loadClientsAndOpenMain() {
        Platform.runLater(() -> setLoadingStatus("Загрузка", "Вход"));

        val refreshToken = launcher.getConfig().get("refreshToken");

        if (refreshToken == null) {
            Platform.runLater(() -> {
                loginPane.setVisible(true);
                loginButton.setDisable(false);
                loginWithOtpButton.setDisable(false);
                loadingOpened.set(false);
                loggedIn.set(false);
            });
            return CompletableFuture.completedFuture(null);
        }

        return launcher.getApi().refreshAccessToken(refreshToken).thenCompose(tokenRefreshResponse -> {
            Platform.runLater(() -> setLoadingStatus("Загрузка", "Обновление списка серверов"));

            session = tokenRefreshResponse.getSession();

            return launcher.getApi().getClients(session.getAccessToken()).thenAccept(clientsResponse ->
                    Platform.runLater(() -> {
                        clients.set(clientsResponse.getClients());
                        loginPane.setVisible(false);
                        //usernameOrEmailTextField.setText("");
                        passwordTextField.setText("");
                        loggedPane.setVisible(true);
                        loadingOpened.set(false);
                        loggedIn.set(true);
                    }));
        }).exceptionally(e -> {
            val launchServerApiException = getBaseLaunchServerApiError(e);

            if (launchServerApiException != null &&
                    launchServerApiException.getMessage().equals(LaunchServerApiException.WRONG_TOKEN)) {
                Platform.runLater(() -> {
                    loginPane.setVisible(true);
                    loginButton.setDisable(false);
                    loginWithOtpButton.setDisable(false);
                    loadingOpened.set(false);
                });
                return null;
            }
            throw SneakyThrow.sneaky(e);
        });
    }

    private static LaunchServerApiException getBaseLaunchServerApiError(Throwable t) {
        while (!(t instanceof LaunchServerApiException)) {
            Throwable cause = t.getCause();
            if (cause == null) return null;
            t = cause;
        }
        return (LaunchServerApiException) t;
    }

    private static Throwable getBaseCause(Throwable t) {
        if (t == null) {
            return null;
        }
        Throwable cause;
        while ((cause = t.getCause()) != null) {
            t = cause;
        }
        return t;
    }

    private void auth() {
        val username = usernameOrEmailTextField.getText();
        if (username.isEmpty()) {
            return;
        }
        val password = passwordTextField.getText();
        if (password.isEmpty()) {
            return;
        }
        Platform.runLater(() -> {
            setLoginError(null);
            setLoginOtpError(null);
            setLoadingStatus("Вход", "");
            loadingOpened.set(true);
            loginButton.setDisable(true);
            loggedIn.set(false);
        });
        launcher.getApi().auth(username, password, otpField.getText())
                .thenCompose(authResponse -> {
                    val config = launcher.getConfig();
                    config.set("refreshToken", authResponse.getRefreshToken());
                    config.set("username", username);
                    Platform.runLater(() -> loggedIn.set(true));
                    return loadClientsAndOpenMain();
                }).exceptionally(e -> {
            e.printStackTrace();
            Platform.runLater(() -> {
                otpField.setText("");
                loadingOpened.set(false);
                loggedIn.set(false);
            });
            val launchServerApiException = getBaseLaunchServerApiError(e);
            if (launchServerApiException != null && launchServerApiException.getMessage()
                    .equals(LaunchServerApiException.OTP_REQUIRED)) {
                Platform.runLater(() -> {
                    otpField.setText("");
                    loadingOpened.set(false);
                    loggedIn.set(false);
                    isOtpOpened.set(true);
                });
                return null;
            }
            Platform.runLater(() -> {
                loginButton.setDisable(false);
                setLoginError(translateThrowable(e));
                setLoginOtpError(translateThrowable(e));
            });
            return null;
        });
    }

    private void play() {
        Platform.runLater(() -> {
            setLoadingStatus("Запуск", "Обновление клиента");
            loadingOpened.set(true);
        });
        ClientInfoData currentClient = clients.get()[currentServerIndex.get()];
        verifyLauncher().thenCompose(__ -> launcher.getApi().getClient(session.getAccessToken(), currentClient.getId()).thenCompose(clientByIdResponse -> {
            val config = launcher.getConfig();
            val signals = clientByIdResponse.getSupportedSignals();
            val clientDirectory = Paths.get(config.get("updatesDirectory"), "clients", currentClient.getId());
            try {
                IOUtil.createDirectoriesIfNotExists(clientDirectory);
            } catch (IOException e) {
                return CompletableFutureUtil.excepted(e);
            }
            val assetsDirectory = Paths.get(config.get("updatesDirectory"), "assets");
            val indexesDirectory = assetsDirectory.resolve("indexes");
            try {
                IOUtil.createDirectoriesIfNotExists(indexesDirectory);
            } catch (IOException e) {
                return CompletableFutureUtil.excepted(e);
            }
            val indexFilePath = indexesDirectory.resolve(TextFormat.jsonPath(currentClient.getId()));
            return FileUtil.getAllFilesAsync(clientDirectory).thenCompose(currentFiles -> {
                Platform.runLater(() -> setLoadingStatus("Запуск", "Обновление клиента: 0%"));
                val assetsDownloadProgress = new AtomicLong(0L);
                val clientDownloadProgress = new AtomicLong(0L);
                val previousProgressPercent = new AtomicInteger();
                Runnable progressUpdateCallback = () -> {
                    val currentProgress = (int) Math.round((Double.longBitsToDouble(assetsDownloadProgress.get()) * 0.3 +
                            Double.longBitsToDouble(clientDownloadProgress.get()) * 0.7) * 100);
                    val previousProgress = previousProgressPercent.get();
                    if (currentProgress == previousProgress) {
                        return;
                    }
                    if (!previousProgressPercent.compareAndSet(previousProgress, currentProgress)) {
                        return;
                    }
                    Platform.runLater(() -> setLoadingStatus("Запуск",
                            String.format("Обновление клиента: %d%%", currentProgress)));
                };
                return CompletableFutureUtil.combine(AssetDownloader.downloadAssets(assetsDirectory, indexFilePath,
                        clientByIdResponse.getAssetFiles(), progress -> {
                            assetsDownloadProgress.set(Double.doubleToRawLongBits(progress));
                            progressUpdateCallback.run();
                        }), FileListVerifier.verifyVerifiedAndGetRequiredAsync(clientDirectory, currentFiles,
                        clientByIdResponse.getClientFiles(),
                        clientByIdResponse.getVerifiedFiles(), clientByIdResponse.getClassPathFiles()).thenCompose(requiredFiles -> {
                    val totalSize = requiredFiles.getRequiredToDownloadFiles().stream().mapToLong(ClientFile::getSize).sum();
                    val totalDownloaded = new AtomicLong(0);
                    return CompletableFuture.allOf(requiredFiles.getRequiredToDownloadFiles().stream().map(clientFile -> {
                        val filePath = clientDirectory.resolve(clientFile.getPath());
                        if (IOUtil.isRegularFile(filePath)) {
                            if (IOUtil.size(filePath) != clientFile.getSize()) {
                                return downloadClientFile(clientDownloadProgress,
                                    progressUpdateCallback, totalSize, totalDownloaded,
                                    clientFile, filePath);
                            }
                        }
                        return HashUtil.getXxHash(filePath).thenCompose(hash -> {
                            if (clientFile.getXxHash().equals(hash)) {
                                totalDownloaded.addAndGet(clientFile.getSize());
                                return CompletableFuture.completedFuture(null);
                            }
                            return downloadClientFile(clientDownloadProgress,
                                progressUpdateCallback, totalSize, totalDownloaded, clientFile,
                                filePath);
                        });
                    }).toArray(CompletableFuture[]::new)).thenApply(_u -> requiredFiles);
                })).thenCompose(result -> {
                    try {
                        config.save(launcher.getConfigPath());
                    } catch (IOException e) {
                        SneakyThrow.sneaky(e);
                    }
                    Platform.runLater(() -> setLoadingStatus("Запуск",
                            "Запуск клиента"));
                    val jvmArgs = new LinkedList<String>();
                    val clientJvmArgs = clientByIdResponse.getJvmArgs();
                    long memoryAmount = config.getLong("memoryAmount", -1L);
                    if (clientJvmArgs != null) {
                        for (int i = 0, j = clientJvmArgs.size(); i < j; i++) {
                            val arg = clientJvmArgs.get(i);
                            if (arg.startsWith("-Xmx")) {
                                val value = arg.substring(4);
                                val modifier = Character.toLowerCase(value.charAt(value.length() - 1));
                                long mem = Long.parseLong(value.substring(0, value.length() - 1));
                                if (modifier == 'g') {
                                    mem *= 1024;
                                }
                                if (memoryAmount < mem) {
                                    memoryAmount = -1L;
                                }
                                break;
                            }
                        }
                        if (memoryAmount >= 0) {
                            jvmArgs.add(TextFormat.xmx(memoryAmount));
                            clientJvmArgs.removeIf(s -> s.startsWith("-Xmx"));
                        }
                        jvmArgs.addAll(clientJvmArgs);
                    }
                    val launchSession = new LauncherSession(session,
                        new LauncherSession.StartupConfig(
                            currentClient.getId(),
                            currentClient.getTitle(),
                            config.get("updatesDirectory"),
                            clientByIdResponse.getMainClass(),
                            jvmArgs,
                            ObjectUtil.firstNotNull(clientByIdResponse.getClientArgs(), Collections.emptyList()),
                            config.getLong("memoryAmount"),
                            config.getBoolean("minimalGraphicSettings"),
                            config.getBoolean("clientFullscreen"),
                            config.getBoolean("disableDiscordRPC"),
                            config.getBoolean("autoEnter"),
                            clientByIdResponse.getVersion(),
                            clientByIdResponse.getAutoLoginHostName(),
                            clientByIdResponse.getAutoLoginPort(),
                            !signals.isEmpty()
                        ),
                        result.getT2().getVerifiedFiles(),
                        result.getT2().getClassPathFiles());

                    Process clientProcess;
                    try {
                        clientProcess = ClientLauncher.start(launchSession);
                    } catch (IOException e) {
                        throw SneakyThrow.sneaky(e);
                    }
                    return CompletableFuture.anyOf(
                        LauncherThreads.run(() -> {
                                try {
                                    if (clientProcess.waitFor(500L, TimeUnit.MILLISECONDS)) {
                                        throw new IllegalStateException("Ошибка запуска клиента");
                                    }
                                } catch (InterruptedException ignored) {
                                }
                            }),
                        LauncherThreads.run(() -> {
                                try {
                                    Platform.runLater(() -> setLoadingStatus("Клиент запущен", ""));
                                    clientProcess.waitFor();
                                    Platform.runLater(() -> loadingOpened.set(false));
                                } catch (Exception ex) {
                                    throw new IllegalStateException(
                                        "Ошибка запуска клиента: " + translateThrowable(ex), ex);
                                }
                            }));
                });
            });
        })).exceptionally(t -> {
            t.printStackTrace();
            Platform.runLater(() -> setLoadingError(translateThrowable(t)));
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ignored) {
            }
            Platform.runLater(() -> loadingOpened.set(false));
            return null;
        });
    }

    private CompletableFuture<Void> downloadClientFile(
        AtomicLong clientDownloadProgress, Runnable progressUpdateCallback,
        long totalSize, AtomicLong totalDownloaded, ClientFile clientFile, Path filePath) {
        try {
            //IOUtil.deleteIfExists(filePath);
            IOUtil.createDirectoriesIfNotExists(filePath.getParent());
        } catch (IOException ex) {
            SneakyThrow.sneaky(ex);
        }
        return Downloader.download(clientFile.getDownloadUrl(), filePath, progress -> {
            clientDownloadProgress.set(Double.doubleToRawLongBits(totalSize == 0 ? 1.0 : 1.0 * totalDownloaded.addAndGet(progress) / totalSize));
            progressUpdateCallback.run();
        });
    }

    private void fetchNews() {
        launcher.getApi().getNews().thenAccept(newsResponse ->
            Platform.runLater(() -> newsItems.set(newsResponse.getNews())))
            .exceptionally(t -> {
                System.err.println("Could not fetch news: ");
                t.printStackTrace();
                return null;
            });
    }

    private CompletableFuture<Void> verifyLauncher() {
        return CompletableFutureUtil.combine(HashUtil.getSha256Hash(JavaUtil.getLauncherPath()),
            launcher.getApi().getLauncherInfo()).thenCompose(result -> {
            val response = result.getT2();
            val serverHash = response.getHash();
            if (!Objects.equals(result.getT1(), serverHash)) {
                Platform.runLater(() -> setLoadingStatus("Загрузка", "Обновление лаунчера: 0%"));
                try {
                    val tempLauncherPath = Files.createTempFile("launcher", ".jar");
                    val launcherDownloadedSize = new AtomicLong();
                    val previousProgressPercent = new AtomicInteger();
                    Runnable progressUpdateCallback = () -> {
                        val currentProgress = result.getT2().getSize() == 0 ? 100 :
                            (int) (100 * launcherDownloadedSize.get() / result.getT2().getSize());
                        val previousProgress = previousProgressPercent.get();
                        if (currentProgress == previousProgress) {
                            return;
                        }
                        if (!previousProgressPercent.compareAndSet(previousProgress, currentProgress)) {
                            return;
                        }
                        Platform.runLater(() -> setLoadingStatus("Загрузка",
                            String.format("Обновление лаунчера: %d%%", currentProgress)));
                    };
                    return Downloader.download(response.getDownloadUrl(), tempLauncherPath, (progress) -> {
                        launcherDownloadedSize.addAndGet(progress);
                        progressUpdateCallback.run();
                    }).thenAccept(_u -> {
                        Platform.runLater(() -> setLoadingStatus("Загрузка", "Перезапуск лаунчера"));
                        saveConfiguration();
                        val path = JavaUtil.getLauncherPath();
                        try {
                            Files.copy(tempLauncherPath, path, StandardCopyOption.REPLACE_EXISTING);
                            Relauncher.relaunch(path, new String[]{"verify", serverHash});
                            Runtime.getRuntime().halt(0);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            Platform.runLater(() -> setLoadingError(translateThrowable(ex)));
                        }
                    });
                } catch (IOException ex) {
                    UnsafeUtil.unsafe().throwException(ex);
                }
            }
            return CompletableFuture.completedFuture(null);
        });
    }

    private void startBootSequence() {
        Platform.runLater(() -> setLoadingStatus("Загрузка", "Обновление новостей и проверка лаунчера"));
        verifyLauncher().thenCompose(__ -> this.loadClientsAndOpenMain()).exceptionally(e -> {
            e.printStackTrace();
            Platform.runLater(() -> setLoadingError(translateThrowable(e)));
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }
            startBootSequence();
            return null;
        });
    }

    private void logout() {
        val config = launcher.getConfig();
        config.set("refreshToken", null);
        Platform.runLater(() -> {
            loggedPane.setVisible(false);
            loginButton.setDisable(false);
            loginWithOtpButton.setDisable(false);
            loggedIn.set(false);
            loginPane.setVisible(true);
            settingsOpened.set(false);
        });
        saveConfiguration();
    }

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainPane.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });
        mainPane.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset);
            stage.setY(event.getScreenY() - yOffset);
        });

        backgroundImage.imageProperty().bind(Bindings.createObjectBinding(() -> new Image(getClass()
                        .getResourceAsStream(TextFormat.backgroundPath(currentBackgroundIndex.get()))),
                currentBackgroundIndex));
        closeButton.setOnAction(__ -> shutdown());
        minimizeButton.setOnAction(action -> stage.setIconified(true));

        bindScrollPaneToScrollBar(newsScrollPane, newsScrollBar, newsFlowPane);
        bindScrollPaneToScrollBar(serversScrollPane, serversScrollBar, serversFlowPane);
        bindScrollPaneToScrollBar(serverInfoScrollPane, serverInfoScrollBar, serverInfoFlowPane);

        newsButton.disableProperty().bind(homeButton.disableProperty().not());

        homeButton.setOnAction(action -> homeButton.setDisable(true));
        newsButton.setOnAction(action -> {
            homeButton.setDisable(false);
        });

        bindAnimation(homeButton.disableProperty(),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(mainNewsPanes.layoutXProperty(), 0, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(mainNewsPanes.layoutXProperty(), -homePane.getPrefWidth(), Interpolator.EASE_BOTH)
                        )
                ))
        );

        settingsPane.setLayoutY(-settingsPane.getPrefHeight());
        settingsButton.setOnAction(action -> settingsOpened.set(settingsOpened.not().get()));
        settingsButton.disableProperty().bind(loadingOpened);

        bindAnimation(settingsOpened,
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.ZERO, event -> settingsButton.setText("НАЗАД")),
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(settingsPane.layoutYProperty(), 0, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.ZERO, event -> settingsButton.setText("НАСТРОЙКИ")),
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(settingsPane.layoutYProperty(), -settingsPane.getPrefHeight(), Interpolator.EASE_BOTH)
                        )
                ))
        );

        startDiamondAnimation();

        val loader = ToolKit.createLoader();
        val backgroundButtonFxml = getClass().getResource("/fxml/background_button.fxml");
        loader.setLocation(backgroundButtonFxml);
        for (int i = 0; i < BACKGROUNDS; i++) {
            loader.setRoot(null);
            loader.setController(null);
            Button button;
            try {
                button = ToolKit.load(loader);
            } catch (IOException ex) {
                throw SneakyThrow.sneaky(ex);
            }
            BackgroundButtonController controller = loader.getController();
            ToolKit.cacheController(controller);

            val $i = i;
            controller.setBackgroundIndex(i);
            button.disableProperty().bind(currentBackgroundIndex.isEqualTo(i));
            button.setOnAction(action -> currentBackgroundIndex.setValue($i));
            backgroundSettingButtonsHBox.getChildren().add(button);
        }
        currentServerIndex.addListener((observable, oldValue, newValue) -> {
            if (currentServerIndex.get() >= cachedNewsImages.size()) {
                return;
            }
            launcher.getConfig().set("lastClient", clients.get()[currentServerIndex.get()].getId());
        });

        val serverFxml = getClass().getResource("/fxml/server.fxml");
        clients.addListener((observable, oldValue, newValue) -> {
            serversFlowPane.getChildren().clear();

            var i = 0;

            val clientsNodes = new ArrayList<Node>(newValue.length);

            loader.setLocation(serverFxml);
            for (val client : newValue) {
                try {
                    loader.setRoot(null);
                    loader.setController(null);
                    Pane pane = ToolKit.load(loader);
                    ServerController controller = loader.getController();
                    ToolKit.cacheController(controller);

                    controller.setOnline(client.isOnline());
                    controller.setMaxOnline(client.getMaxPlayers());
                    controller.setCurrentOnline(client.getCurrentPlayers());
                    controller.setImageId(client.getImageId());
                    controller.setTitle(client.getTitle());

                    val finalI = i;
                    pane.setOnMouseClicked(event -> currentServerIndex.set(finalI));

                    controller.selectedProperty().bind(currentServerIndex.isEqualTo(i));

                    val isVisibleProperty = showFavouriteOnlyButton.selectedProperty().not()
                            .or(controller.favouriteProperty());

                    controller.setFavourite(launcher.getConfig().getBoolean(
                            TextFormat.favourite(client.getId())));

                    controller.favouriteProperty().addListener((favouriteObservable,
                                                                favouriteOldValue, favouriteNewValue) ->
                            launcher.getConfig().setBoolean(TextFormat.favourite(client.getId()),
                                    favouriteNewValue));

                    pane.visibleProperty().bind(client.isTechnical() ? isVisibleProperty
                            .and(showTechnicalServersButton.selectedProperty()) : isVisibleProperty);
                    pane.managedProperty().bind(pane.visibleProperty());

                    clientsNodes.add(pane);
                } catch (IOException ignored) {
                }

                i++;
            }

            serversFlowPane.getChildren().addAll(clientsNodes.toArray(new Node[0]));
        });

        clients.addListener((observable, oldValue, newValue) -> {
            cachedNewsImages.clear();
            for (val client : newValue) {
                cachedNewsImages.add(new Image(client.getLastNews().getImageUrl(), true));
            }
        });

        clients.addListener((observable, oldValue, newValue) -> {
            val lastClient = launcher.getConfig().get("lastClient");
            if (lastClient == null) {
                return;
            }

            for (var i = 0; i < newValue.length; i++) {
                if (newValue[i].getId().equals(lastClient)) {
                    currentServerIndex.set(i);
                    return;
                }
            }
            currentServerIndex.set(0);
        });

        val newsFxml = getClass().getResource("/fxml/news.fxml");
        newsItems.addListener((observable, oldValue, newValue) -> {
            newsFlowPane.getChildren().clear();

            val newsNodes = new ArrayList<Node>();

            loader.setLocation(newsFxml);
            for (val item : newValue) {
                try {
                    loader.setRoot(null);
                    loader.setController(null);
                    Pane pane = ToolKit.load(loader);
                    NewsController controller = loader.getController();
                    ToolKit.cacheController(controller);
                    controller.setImageUrl(new URL(item.getImageUrl()));
                    controller.setNewsText(item.getText());
                    controller.setTimestamp(new Date(item.getTimestamp() * 1000));
                    try {
                        controller.setUri(new URI(item.getUrl()));
                    } catch (URISyntaxException ignored) {
                    }

                    newsNodes.add(pane);
                } catch (IOException ex) {
                    System.err.println("Could not add news pane: ");
                    ex.printStackTrace();
                }
            }

            newsFlowPane.getChildren().addAll(newsNodes);
        });

        serverNewsImageView.imageProperty().bind(Bindings.createObjectBinding(() -> {
            if (currentServerIndex.get() >= cachedNewsImages.size()) {
                return null;
            }
            return cachedNewsImages.get(currentServerIndex.get());
        }, currentServerIndex, clients, cachedNewsImages));
        serverNewsTitleLabel.textProperty().bind(Bindings.createStringBinding(() -> {
            if (currentServerIndex.get() >= clients.get().length) {
                return null;
            }
            return clients.get()[currentServerIndex.get()].getLastNews().getTitle();
        }, currentServerIndex, clients));
        serverNewsTextLabel.textProperty().bind(Bindings.createStringBinding(() -> {
            if (currentServerIndex.get() >= clients.get().length) {
                return null;
            }
            return clients.get()[currentServerIndex.get()].getLastNews().getText();
        }, currentServerIndex, clients));
        serverInfoLabel.textProperty().bind(Bindings.createStringBinding(() -> {
            if (currentServerIndex.get() >= clients.get().length) {
                return null;
            }
            return clients.get()[currentServerIndex.get()].getInformation();
        }, currentServerIndex, clients));

        scrollServersPaneStateButton.disableProperty().bind(gridServersPaneStateButton.disableProperty().not());

        scrollServersPaneStateButton.setOnAction(actionEvent -> gridServersPaneStateButton.setDisable(false));
        gridServersPaneStateButton.setOnAction(actionEvent -> gridServersPaneStateButton.setDisable(true));

        bindAnimation(scrollServersPaneStateButton.disableProperty(),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(290),
                                new KeyValue(informationPane.opacityProperty(), 1, Interpolator.EASE_BOTH),
                                new KeyValue(serverNewsPane.opacityProperty(), 1, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollBar.opacityProperty(), 1, Interpolator.EASE_BOTH),
                                new KeyValue(serversPaneBackground.opacityProperty(), 1, Interpolator.EASE_BOTH),
                                new KeyValue(playButton.layoutXProperty(), 285, Interpolator.EASE_BOTH),
                                new KeyValue(playButton.prefWidthProperty(), 193, Interpolator.EASE_BOTH),
                                new KeyValue(serversPaneBackground.prefWidthProperty(), 266, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollPane.prefWidthProperty(), 201, Interpolator.EASE_BOTH),
                                new KeyValue(serversFlowPane.maxWidthProperty(), 201, Interpolator.EASE_BOTH),
                                new KeyValue(serversFlowPane.prefWidthProperty(), 201, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollPane.layoutXProperty(), 38, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollBar.layoutXProperty(), 255, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(290),
                                new KeyValue(informationPane.opacityProperty(), 0, Interpolator.EASE_BOTH),
                                new KeyValue(serverNewsPane.opacityProperty(), 0, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollBar.opacityProperty(), 0, Interpolator.EASE_BOTH),
                                new KeyValue(serversPaneBackground.opacityProperty(), 0.3, Interpolator.EASE_BOTH),
                                new KeyValue(playButton.layoutXProperty(), 364, Interpolator.EASE_BOTH),
                                new KeyValue(playButton.prefWidthProperty(), 227, Interpolator.EASE_BOTH),
                                new KeyValue(serversPaneBackground.prefWidthProperty(), homePane.getPrefWidth(), Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollPane.prefWidthProperty(), 1065, Interpolator.EASE_BOTH),
                                new KeyValue(serversFlowPane.maxWidthProperty(), 1065, Interpolator.EASE_BOTH),
                                new KeyValue(serversFlowPane.prefWidthProperty(), 1065, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollPane.layoutXProperty(), 73, Interpolator.EASE_BOTH),
                                new KeyValue(serversScrollBar.layoutXProperty(), 1189, Interpolator.EASE_BOTH)
                        )
                ))
        );

        loadingOpened.set(true);
        bindAnimation(loadingOpened,
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(loadingPane.layoutYProperty(), 0, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(loadingPane.layoutYProperty(), loadingPane.getPrefHeight(), Interpolator.EASE_BOTH)
                        )
                ))
        );

        bindAnimation(isOtpOpened,
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(400),
                                new KeyValue(loginOtpPanes.layoutXProperty(), -loginInputPane.getPrefWidth(), Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable() {
                    @Override
                    public void start() {
                        loginOtpPanes.setLayoutX(0);
                    }

                    @Override
                    public void stop() {
                    }
                }
        );

        playButton.setOnAction(action -> play());

        loginPane.visibleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                otpField.setText("");
                isOtpOpened.set(false);
            }
        });

        bindAnimation(loginErrorShown,
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(200),
                                new KeyValue(loginErrorPane.opacityProperty(), 1, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(200),
                                new KeyValue(loginErrorPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                        )
                ))
        );

        bindAnimation(loginOtpErrorShown,
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(200),
                                new KeyValue(loginOtpErrorPane.opacityProperty(), 1, Interpolator.EASE_BOTH)
                        )
                )),
                new IStartStoppable.AnimationStartStoppable(new Timeline(60,
                        new KeyFrame(Duration.millis(200),
                                new KeyValue(loginOtpErrorPane.opacityProperty(), 0, Interpolator.EASE_BOTH)
                        )
                ))
        );

        usernameOrEmailTextField.disableProperty().bind(loginButton.disableProperty());
        passwordTextField.disableProperty().bind(loginButton.disableProperty());
        otpField.disableProperty().bind(loginWithOtpButton.disableProperty());
        usernameOrEmailTextField.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
        passwordTextField.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);
        usernameOrEmailTextField.setTextFormatter(new TextFormatter<>(change -> {
            if (NameMatcherHolder.matcher.reset(change.getControlNewText()).matches()) {
                return change;
            }
            return null;
        }));
        logoImage.onMouseClickedProperty().set(e -> {
            if (settingsOpened.get()) {
                settingsOpened.set(false);
            }
            homeButton.setDisable(true);
        });
        autoMemoryCheckBox.disableProperty().bind(loggedIn.not());
        val newsScrolledDown = newsScrollBar.valueProperty().isEqualTo(0);
        logoImage.visibleProperty().bind(newsScrolledDown);
        homeButton.visibleProperty().bind(newsScrolledDown);
        newsButton.visibleProperty().bind(newsScrolledDown);
        settingsButton.visibleProperty().bind(newsScrolledDown);
        SmoothScroll.enable(newsScrollPane);
        loadingOpened.addListener((__, ___, v) -> {
            if (v) {
                diamondAnimation.play();
            } else {
                diamondAnimation.stop();
            }
        });
    }

    private void saveConfiguration() {
        try {
            launcher.getConfig().save(launcher.getConfigPath());
        } catch (IOException e) {
            ExceptionAlert.show(e, "Сохранение конфигурации");
        }
    }

    private void shutdown() {
        saveConfiguration();
        Runtime.getRuntime().halt(0);
    }

    public void postInitialize() {
        stage.setOnCloseRequest(event -> {
            shutdown();
        });

        try {
            val backgroundIndexString = launcher.getConfig().get("backgroundIndex");
            if (backgroundIndexString != null) {
                currentBackgroundIndex.set(Integer.parseInt(backgroundIndexString));
            }
        } catch (NumberFormatException e) {
            currentBackgroundIndex.set(0);
        }

        Config config = launcher.getConfig();
        currentBackgroundIndex.addListener((observable, oldValue, newValue) ->
            config.set("backgroundIndex", newValue.toString()));

        gridServersPaneStateButton.setDisable(config.getBoolean("isScrollUsed"));
        gridServersPaneStateButton.disableProperty().addListener((observable, oldValue, newValue) ->
            config.setBoolean("isScrollUsed", newValue));

        loginButton.setOnAction(action -> auth());
        loginWithOtpButton.setOnAction(action -> auth());

        logoutButton.disableProperty().bind(loggedIn.not());
        logoutButton.setOnAction(action -> logout());

        val totalMemory = JavaUtil.getTotalMemory();
        val memoryAmount = Math.min(config.getLong("memoryAmount", -1L), totalMemory);
        autoMemoryCheckBox.setSelected(memoryAmount < 512);
        Slider memorySlider = this.memorySlider;
        memorySlider.setMin(512);
        memorySlider.setMax(Math.max(512, totalMemory));
        memorySlider.setValue(memoryAmount);
        memorySlider.disableProperty().bind(loggedIn.not().or(autoMemoryCheckBox.selectedProperty()));
        config.set("memoryAmount", Long.toString(memoryAmount));
        memoryForGameLabel.textProperty().bind(Bindings.createStringBinding(() ->
                        TextFormat.ram(selectedMemoryValue.get()),
                selectedMemoryValue));
        selectedMemoryValue.set(memoryAmount);
        val memorySliderValue = new SimpleDoubleProperty(memoryAmount);
        selectedMemoryValue.bind(Bindings.when(autoMemoryCheckBox.selectedProperty()).then(-1).otherwise(
                Bindings.createLongBinding(() -> {
                    val value = Math.round(memorySliderValue.get());
                    return value == -1L ? 512L : value;
                }, memorySliderValue)));
        selectedMemoryValue.addListener((observable, oldValue, newValue) ->
                launcher.getConfig().set("memoryAmount", newValue.toString()));
        memorySlider.valueProperty().addListener((property, oldValue, newValue) -> {
            var value = newValue.doubleValue();
            for (int i = 0; i < (totalMemory + 511) / 512; i++)
                if (Math.abs(value - 512 * i) < 150)
                    value = 512 * i;
            ((DoubleProperty) property).set(value);
            memorySliderValue.set(value);
        });

        BooleanBinding loggedInNot = loggedIn.not();
        minimalGraphicSettingsCheckBox.disableProperty().bind(loggedInNot);
        disableDiscordRPCCheckBox.disableProperty().bind(loggedInNot);
        clientFullscreenCheckBox.disableProperty().bind(loggedInNot);
        autoEnterCheckBox.disableProperty().bind(loggedInNot);
        bindCheckBoxToSetting(minimalGraphicSettingsCheckBox, "minimalGraphicSettings");
        bindCheckBoxToSetting(disableDiscordRPCCheckBox, "disableDiscordRPC");
        bindCheckBoxToSetting(clientFullscreenCheckBox, "clientFullscreen");
        bindCheckBoxToSetting(autoEnterCheckBox, "autoEnter");

        if (config.get("updatesDirectory") == null) {
            config.set("updatesDirectory", C7XHome.resolve("updates").toString());
        }
        try {
            IOUtil.createDirectoriesIfNotExists(Paths.get(config.get("updatesDirectory")));
        } catch (IOException e) {
            ExceptionAlert.show(e, "Создание директории");
            stage.close();
            return;
        }

        wipeClientButton.disableProperty().bind(loggedIn.not());
        wipeClientButton.setOnAction(action -> {
            val clients = this.clients.get();
            if (currentServerIndex.get() >= clients.length) {
                return;
            }
            val clientInfoData = clients[currentServerIndex.get()];
            setLoadingStatus("Удаление", TextFormat.clientDelete(clientInfoData.getTitle()));
            loadingOpened.set(true);
            FileUtil.removeDirectoryAsync(Paths.get(config.get("updatesDirectory"), "clients",
                    clientInfoData.getId()))
                    .thenAccept(_u -> Platform.runLater(() -> loadingOpened.set(false)))
                    .exceptionally(t -> {
                        Platform.runLater(() -> setLoadingError(translateThrowable(t)));
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ignored) {
                        }
                        Platform.runLater(() -> loadingOpened.set(false));
                        return null;
                    });
        });
        openGameDirectoryButton.disableProperty().bind(loggedIn.not());
        openGameDirectoryButton.setOnAction(action -> {
            try {
                DocumentUtil.showDocument(config.get("updatesDirectory"));
            } catch (IOException e) {
                ExceptionAlert.show(e, "Открытие директории игры");
            }
            //Desktop.getDesktop().open(new File(launcher.getConfig().get("updatesDirectory")));
        });
        setGameDirectoryButton.disableProperty().bind(loggedIn.not());
        setGameDirectoryButton.setOnAction(action -> {
            val currentDirectory = Paths.get(config.get("updatesDirectory"));
            val directoryChooserDialog = new DirectoryChooser();
            directoryChooserDialog.setInitialDirectory(currentDirectory.toFile());
            directoryChooserDialog.setTitle("Выберите папку для файлов игры");
            val result = directoryChooserDialog.showDialog(stage);
            if (result == null) {
                return;
            }
            val newDirectory = result.toPath();
            try {
                if (Files.list(newDirectory).count() > 0) {
                    ExceptionAlert.show(null, "Выбранная папка пуста");
                    return;
                }
            } catch (IOException e) {
                ExceptionAlert.show(e, "Провверка директории");
                return;
            }
            val newUpdatesDirectory = newDirectory.resolve("updates");
            setLoadingStatus("Перенос", "Перенос файлов игры");
            loadingOpened.set(true);
            FileUtil.moveDirectoryAsync(currentDirectory, newUpdatesDirectory)
                    .thenAccept(_u -> {
                        config.set("updatesDirectory", newUpdatesDirectory.toString());
                        Platform.runLater(() -> loadingOpened.set(false));
                    })
                    .exceptionally(t -> {
                        Platform.runLater(() -> setLoadingError(translateThrowable(t)));
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ignored) {
                        }
                        Platform.runLater(() -> loadingOpened.set(false));
                        return null;
                    });
        });

        val username = config.get("username");
        if (username != null && NameMatcherHolder.matcher.reset(username).matches()) {
            usernameOrEmailTextField.setText(username);
        }
        fetchNews();
        startBootSequence();
    }

    private void bindCheckBoxToSetting(CheckBox checkBox, String setting) {
        val config = launcher.getConfig();
        checkBox.setSelected(config.getBoolean(setting));
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) ->
                config.setBoolean(setting, newValue));
    }

    private void startDiamondAnimation() {
        List<Animation> animations = new ArrayList<>(3);

        {
            double totalDuration = 1340;

            Animation animation = new Timeline(60,
                    new KeyFrame(Duration.millis(totalDuration).multiply(0),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.15),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 149.0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.30),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 25.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.45),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 25.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.60),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 62.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.70),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 62.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(1),
                            new KeyValue(cristalixDiamond2.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond2.layoutYProperty(), 62.5, Interpolator.EASE_BOTH)
                    )
            );

            animation.setCycleCount(Animation.INDEFINITE);

            animations.add(new SequentialTransition(
                    new PauseTransition(Duration.millis(400)),
                    animation
            ));
        }

        {
            double totalDuration = 1340;

            Animation animation = new Timeline(60,
                    new KeyFrame(Duration.millis(totalDuration).multiply(0),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 112.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.15),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 149.0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.30),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 25.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.45),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 25.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.60),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.66),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 112.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(1),
                            new KeyValue(cristalixDiamond3.layoutXProperty(), 125.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond3.layoutYProperty(), 112.5, Interpolator.EASE_BOTH)
                    )
            );

            animation.setCycleCount(Animation.INDEFINITE);

            animations.add(animation);
        }

        {
            double totalDuration = 1340;

            Animation animation = new Timeline(60,
                    new KeyFrame(Duration.millis(totalDuration).multiply(0),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.15),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 149.0, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.30),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 25.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.45),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 87.5, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 25.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.68),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(0.90),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    ),
                    new KeyFrame(Duration.millis(totalDuration).multiply(1),
                            new KeyValue(cristalixDiamond4.layoutXProperty(), 149.0, Interpolator.EASE_BOTH),
                            new KeyValue(cristalixDiamond4.layoutYProperty(), 87.5, Interpolator.EASE_BOTH)
                    )
            );

            animation.setCycleCount(Animation.INDEFINITE);

            animations.add(new SequentialTransition(
                    new PauseTransition(Duration.millis(200)),
                    animation
            ));
        }

        this.diamondAnimation = new ParallelTransition(animations.toArray(new Animation[0]));
    }

    private static void bindScrollPaneToScrollBar(ScrollPane scrollPane, ScrollBar scrollBar, Region contents) {
        scrollBar.setMax(1);
        scrollBar.visibleAmountProperty().bind(scrollPane.heightProperty().divide(contents.heightProperty()));
        scrollPane.vvalueProperty().bindBidirectional(scrollBar.valueProperty());
        scrollBar.visibleProperty().bind(scrollBar.visibleAmountProperty().lessThanOrEqualTo(1));
    }

    private void bindAnimation(ObservableValue<Boolean> inputProperty, IStartStoppable inStartStoppable,
                               IStartStoppable outStartStoppable) {
        AtomicReference<IStartStoppable> currentStartStoppable = new AtomicReference<>();
        inputProperty.addListener((observable, oldValue, newValue) -> {
            if ((!oldValue && newValue) || (oldValue && !newValue)) {
                val current = currentStartStoppable.get();
                if (current != null) {
                    current.stop();
                }

                val $newValue = oldValue ? outStartStoppable : inStartStoppable;
                currentStartStoppable.set($newValue);
                $newValue.start();
            }
        });
    }
}