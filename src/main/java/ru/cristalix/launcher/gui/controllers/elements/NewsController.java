package ru.cristalix.launcher.gui.controllers.elements;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ru.cristalix.launcher.utils.DocumentUtil;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class NewsController implements Initializable {

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy 'в' HH:mm");

    @FXML
    private ImageView mainImageView;
    @FXML
    private Label timeLabel;
    @FXML
    private Label textLabel;
    @FXML
    private Hyperlink mainHyperlink;

    private final ObjectProperty<URL> imageUrl = new SimpleObjectProperty<>();

    public URL getImageUrl() {
        return imageUrl.get();
    }

    public ObjectProperty<URL> imageUrlProperty() {
        return imageUrl;
    }

    public void setImageUrl(URL imageUrl) {
        this.imageUrl.set(imageUrl);
    }

    private final ObjectProperty<Date> timestamp = new SimpleObjectProperty<>();

    public Date getTimestamp() {
        return timestamp.get();
    }

    public ObjectProperty<Date> timestampProperty() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp.set(timestamp);
    }

    private final StringProperty text = new SimpleStringProperty();

    public String getNewsText() {
        return text.get();
    }

    public StringProperty newsTextProperty() {
        return text;
    }

    public void setNewsText(String newsText) {
        this.text.set(newsText);
    }

    private final ObjectProperty<URI> uri = new SimpleObjectProperty<>();

    public URI getUri() {
        return uri.get();
    }

    public ObjectProperty<URI> uriProperty() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri.set(uri);
    }

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        mainImageView.imageProperty().bind(Bindings.createObjectBinding(() -> {
            if (imageUrl.get() == null) {
                return null;
            }
            return new Image(imageUrl.get().toString(), true);
        }, imageUrl));
        timeLabel.textProperty().bind(Bindings.createStringBinding(() ->
                timestamp.get() == null ? "" : DATE_FORMAT.format(timestamp.get()), timestamp));
        textLabel.textProperty().bind(text);
        mainHyperlink.visitedProperty().bind(new ReadOnlyBooleanWrapper(false));
        mainHyperlink.setOnAction(actionEvent -> {
            try {
                DocumentUtil.showDocument(uri.get().toString());
            } catch (IOException e) {
                System.err.println("Could ont open news link: ");
                e.printStackTrace();
            }
        });
    }
}
