package ru.cristalix.launcher.gui;

import javafx.animation.Animation.Status;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.event.EventHandler;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public final class SmoothScroll {

  private final static int TRANSITION_DURATION = 200;
  private final static double BASE_MODIFIER = 1;

  public static void enable(ScrollPane pane) {
    VBox wrapper = new VBox(pane.getContent());
    pane.setContent(wrapper);
    wrapper.setOnScroll(new EventHandler<ScrollEvent>() {
      private SmoothishTransition transition;

      @Override
      public void handle(ScrollEvent event) {
        double deltaY = BASE_MODIFIER * event.getDeltaY();
        double width = pane.getContent().getBoundsInLocal().getWidth();
        double vvalue = pane.getVvalue();
        Interpolator interp = Interpolator.LINEAR;
        transition = new SmoothishTransition(transition, deltaY) {
          @Override
          protected void interpolate(double frac) {
            double x = interp.interpolate(vvalue, vvalue + -deltaY * getMod() / width, frac);
            pane.setVvalue(x);
          }
        };
        transition.play();
      }
    });
  }

  private static boolean playing(Transition t) {
    return t.getStatus() == Status.RUNNING;
  }

  private static boolean sameSign(double d1, double d2) {
    return (d1 > 0 && d2 > 0) || (d1 < 0 && d2 < 0);
  }

  private abstract static class SmoothishTransition extends Transition {

    private final double mod;
    private final double delta;

    public SmoothishTransition(SmoothishTransition old, double delta) {
      setCycleDuration(Duration.millis(TRANSITION_DURATION));
      setCycleCount(0);
      if (old != null && sameSign(delta, old.delta) && playing(old)) {
        mod = old.getMod() + 1;
      } else {
        mod = 1;
      }
      this.delta = delta;
    }

    public double getMod() {
      return mod;
    }

    @Override
    public void play() {
      super.play();
      if (getMod() > 1) {
        jumpTo(getCycleDuration().divide(10));
      }
    }
  }
}