package ru.cristalix.launcher.gui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.val;
import ru.cristalix.launcher.Launcher;
import ru.cristalix.launcher.gui.controllers.MainController;

public class FXApplication {

    public static void start(Launcher launcher, Stage primaryStage) throws Exception {
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        val loader = ToolKit.LOADER;
        loader.setLocation(Launcher.class.getResource("/fxml/main.fxml"));
        Pane root = ToolKit.load(loader);
        loader.setRoot(null);
        val scene = new Scene(root, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Launcher.class.getResourceAsStream("/images/icons/icon.png")));
        MainController controller = loader.getController();
        controller.setLauncher(launcher);
        controller.setStage(primaryStage);
        controller.postInitialize();
        ToolKit.cacheController(controller);
        primaryStage.show();
        primaryStage.setTitle("Cristalix");
    }
}
