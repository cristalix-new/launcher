package ru.cristalix.launcher.starter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.cristalix.launcher.api.data.ClientFile;
import ru.cristalix.launcher.api.data.SessionData;

import java.util.Collection;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class LauncherSession {
    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    public static class StartupConfig {
        String clientId;
        String title;
        String updatesDirectory;
        String mainClass;
        Collection<String> jvmArgs;
        Collection<String> clientArgs;
        long memoryAmount;
        boolean minimalGraphicSettings;
        boolean clientFullscreen;
        boolean disableDiscordRPC;
        boolean autoEnter;
        String version;
        String autoLoginHostName;
        int autoLoginPort;
        boolean startLocalServer;
    }

    SessionData session;
    StartupConfig startupConfig;
    Collection<ClientFile> verifiedFiles;
    Collection<ClientFile> classPathFiles;
}
