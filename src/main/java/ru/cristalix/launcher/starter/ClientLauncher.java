package ru.cristalix.launcher.starter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mojang.authlib.yggdrasil.YggdrasilMinecraftSessionService;
import lombok.val;
import ru.cristalix.launcher.utils.GsonInstance;
import ru.cristalix.launcher.utils.JavaUtil;
import ru.cristalix.launcher.utils.MacUtil;
import ru.cristalix.launcher.utils.Version;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Paths;
import java.util.LinkedList;

public final class ClientLauncher {

    public static Process start(LauncherSession launcherSession) throws IOException {
        val startupConfig = launcherSession.getStartupConfig();
        val updatesDir = Paths.get(startupConfig.getUpdatesDirectory());
        val assetsDirectoryPath = updatesDir.resolve("assets");
        val shortClientDirectoryPath = Paths.get("clients", startupConfig.getClientId());
        val clientDirectoryPath = updatesDir.resolve(shortClientDirectoryPath);
        val builder = new ProcessBuilder()
                .directory(clientDirectoryPath.toFile())
                .redirectErrorStream(true)
                .redirectOutput(Redirect.INHERIT);
        val command = new LinkedList<String>();
        command.add(JavaUtil.getJavaPath().toString());
        command.addAll(startupConfig.getJvmArgs());
        command.add("-cp");
        val launcherPath = JavaUtil.getLauncherPath().toString();
        command.add(launcherPath);
        val nativesDirectoryPath = clientDirectoryPath.resolve("natives");
        if (MacUtil.FUCK_MAC) {
            command.add("-Dapple.awt.application.name=" + startupConfig.getTitle());
        }
        command.add(ClientStarter.class.getName());
        val session = launcherSession.getSession();
        val profile = session.getProfile();
        val process = builder.command(command).start();
        try (val os = new DataOutputStream(process.getOutputStream())) {
            os.writeUTF(startupConfig.getMainClass()); // main
            os.writeUTF(nativesDirectoryPath.toAbsolutePath().toString()); // natives
            val cp = launcherSession.getClassPathFiles(); // classpath
            // Write classpath
            for (val cf : cp) {
                os.writeUTF(cf.getPath());
            }
            os.writeShort(0);
            // Write user info
            writeClientArgs(os, "--username", profile.getUsername());
            writeClientArgs(os, "--uuid", profile.getUuid().toString().replace("-", ""));

            val accessToken = session.getAccessToken();
            val ver = Version.parse(startupConfig.getVersion());
            if (ver.compareTo(new Version(1, 7, 10)) >= 0) {
                writeClientArgs(os, "--accessToken", accessToken);
                // Write user props
                {
                    val userProperties = new JsonObject();
                    val $skin = profile.getSkin();
                    if ($skin != null) {
                        val skin = new JsonArray();
                        skin.add($skin.getUrl());
                        userProperties.add(YggdrasilMinecraftSessionService.SKIN_URL_PROPERTY, skin);
                        val digest = new JsonArray();
                        digest.add($skin.getSha256());
                        userProperties.add(YggdrasilMinecraftSessionService.SKIN_DIGEST_PROPERTY, digest);
                    }
                    val $cape = profile.getCape();
                    if ($cape != null) {
                        val cloak = new JsonArray();
                        cloak.add($cape.getUrl());
                        userProperties.add(YggdrasilMinecraftSessionService.CAPE_URL_PROPERTY, cloak);
                        val digest = new JsonArray();
                        digest.add($cape.getSha256());
                        userProperties.add(YggdrasilMinecraftSessionService.CAPE_DIGEST_PROPERTY, digest);
                    }
                    writeClientArgs(os, "--userProperties", GsonInstance.GSON.toJson(userProperties));
                }
                writeClientArgs(os, "--assetIndex", startupConfig.getClientId());
            } else {
                writeClientArgs(os, "--session", accessToken);
            }
            writeClientArgs(os, "--version", startupConfig.getVersion());
            writeClientArgs(os, "--assetsDir", assetsDirectoryPath.toAbsolutePath().toString());
            // Write auto-logon info
            if (startupConfig.isAutoEnter()) {
                val loginHostName = startupConfig.getAutoLoginHostName();
                if (loginHostName != null) {
                    writeClientArgs(os, "--server", loginHostName);
                    writeClientArgs(os, "--port", Integer.toString(startupConfig.getAutoLoginPort()));
                }
            }
            // Write whether the client should start in fullscreen or not
            if (startupConfig.isClientFullscreen()) {
                writeClientArgs(os, "--fullscreen", Boolean.toString(true));
            }
            // Write the rest of arguments
            val clientArgs = startupConfig.getClientArgs();
            for (val arg : clientArgs) {
                os.writeUTF(arg);
            }
            os.writeShort(0); // END
        }
        return process;
    }

    private static void writeClientArgs(DataOutputStream os, String a, String b) throws IOException {
        os.writeUTF(a);
        os.writeUTF(b);
    }

    private static void writeClientArgs(DataOutputStream os, String a, String b, String c) throws IOException {
        os.writeUTF(a);
        os.writeUTF(b);
        os.writeUTF(c);
    }
}
