package ru.cristalix.launcher.starter;

import lombok.SneakyThrows;
import lombok.val;
import sun.reflect.ReflectionFactory;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;

public class ClientStarter {

  @SneakyThrows
  public static void main(String[] $args) {
    val dis = new DataInputStream(System.in);
    val main = dis.readUTF();
    val libraryPath = dis.readUTF();
    {
      System.setProperty("java.library.path",
          System.getProperty("java.library.path") + File.pathSeparator + libraryPath);
      val field = ClassLoader.class.getDeclaredField("sys_paths");
      field.setAccessible(true);
      field.set(null, null);
    }
    val loader = ClientStarter.class.getClassLoader();
    {
      val m = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
      m.setAccessible(true);
      val accessor = ReflectionFactory.getReflectionFactory().newMethodAccessor(m);
      val field = accessor.getClass().getDeclaredField("numInvocations");
      field.setAccessible(true);
      field.setInt(accessor, Integer.MIN_VALUE);
      val args = new Object[1];
      val fs = FileSystems.getDefault();
      val more = new String[0];
      while (true) {
        val arg = dis.readUTF();
        if (arg.isEmpty()) {
          break;
        }
        args[0] = fs.getPath(arg, more).toUri().toURL();
        accessor.invoke(loader, args);
      }
    }
    {
      val args = new ArrayList<String>();
      while (true) {
        val arg = dis.readUTF();
        if (arg.isEmpty()) {
          break;
        }
        args.add(arg);
      }
      args.addAll(Arrays.asList("--userType", "mojang", "--gameDir", ".",  "--resourcePackDir", "resourcepacks"));
      try {
        dis.close();
      } catch (IOException ignored) {
      }
      Class.forName(main, true, loader).getMethod("main", String[].class)
              .invoke(null, new Object[]{args.toArray(new String[0])});
    }
  }
}
