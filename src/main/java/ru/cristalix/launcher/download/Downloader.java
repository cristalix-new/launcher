package ru.cristalix.launcher.download;

import lombok.val;
import lombok.var;
import ru.cristalix.launcher.utils.IOUtil;
import ru.cristalix.launcher.utils.SneakyThrow;
import ru.cristalix.launcher.utils.ThreadFactoryBuilder;

import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class Downloader {

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(8, new ThreadFactoryBuilder().setNameFormat("Downloader #").build());

    public static CompletableFuture<Void> download(String url, Path downloadPath,
        Consumer<Integer> downloadedConsumer) {
        return CompletableFuture.runAsync(() -> {
            try {
                val connection = IOUtil.get(url);
                try (val inputStream = connection.getInputStream();
                    val outputStream = IOUtil.newOutputStream(downloadPath)) {
                    var read = 0;
                    val buffer = IOUtil.byteBuffer();
                    while ((read = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, read);
                        downloadedConsumer.accept(read);
                    }
                    downloadedConsumer.accept(0);
                }
            } catch (Exception e) {
                throw SneakyThrow.sneaky(e);
            }
        }, EXECUTOR);
    }
}
