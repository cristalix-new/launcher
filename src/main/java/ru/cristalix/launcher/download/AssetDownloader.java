package ru.cristalix.launcher.download;

import com.google.gson.annotations.SerializedName;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import ru.cristalix.launcher.api.data.AssetFile;
import ru.cristalix.launcher.utils.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AssetDownloader {

    private static Path getAssetPath(Path objectsDir, String hash) {
        return objectsDir.resolve(hash.substring(0, 2)).resolve(hash);
    }

    @RequiredArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    @Getter
    private static class MojangAssetIndexFile {
        @RequiredArgsConstructor
        @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
        @Getter
        private static class ObjectData {
            @SerializedName("hash")
            String hash;
            @SerializedName("size")
            long size;
        }

        @SerializedName("objects")
        Map<String, ObjectData> objects;
    }

    private static CompletableFuture<Void> saveAssetsToIndexFile(Path indexFile, AssetFile[] assetFiles) {
        return LauncherThreads.run(() -> {
            try (val writer = IOUtil.writer(indexFile, StandardCharsets.UTF_8)) {
                GsonInstance.GSON.toJson(new MojangAssetIndexFile(Arrays.stream(assetFiles)
                    .collect(Collectors.toMap(AssetFile::getPath,
                        assetFile -> new MojangAssetIndexFile.ObjectData(assetFile.getSha256(), assetFile.getSize())))), writer);
            } catch (Exception e) {
                SneakyThrow.sneaky(e);
            }
        });
    }

    public static CompletableFuture<Void> downloadAssets(Path assetsDirectory, Path indexFile, AssetFile[] assetFiles,
                                                         Consumer<Double> progressConsumer) {
        val totalDownloaded = new AtomicLong();
        val totalSize = size(assetFiles);
        val objectsDirectory = assetsDirectory.resolve("objects");
        try {
            IOUtil.createDirectoriesIfNotExists(objectsDirectory);
        } catch (IOException ex) {
            return CompletableFutureUtil.excepted(ex);
        }
        return CompletableFuture.allOf(merge(saveAssetsToIndexFile(indexFile, assetFiles), Arrays.stream(assetFiles).map(assetFile -> {
            val assetPath = getAssetPath(objectsDirectory, assetFile.getSha256());
            val size = assetFile.getSize();
            if (IOUtil.size(assetPath) == size) {
                val totalNow = totalDownloaded.addAndGet(size);
                progressConsumer.accept(1.0 * totalNow / totalSize);
                return CompletableFuture.completedFuture(null);
            }
            try {
                //IOUtil.deleteIfExists(assetPath);
                IOUtil.createDirectoryIfNotExists(assetPath.getParent());
            } catch (Exception ex) {
                SneakyThrow.sneaky(ex);
            }
            //val downloadedForFile = new AtomicLong();
            return Downloader.download(assetFile.getDownloadUrl(), assetPath,
                downloaded -> {
                    val totalNow = totalDownloaded.addAndGet(downloaded);
                    progressConsumer.accept(1.0 * totalNow / totalSize);
                });
        }).toArray(CompletableFuture[]::new)));
    }

    private static CompletableFuture<?>[] merge(CompletableFuture<?> future, CompletableFuture<?>[] with) {
        CompletableFuture<?>[] futures = new CompletableFuture[with.length + 1];
        futures[0] = future;
        System.arraycopy(with, 0, futures, 1, with.length);
        return futures;
    }

    private static long size(AssetFile[] files) {
        long size = 0L;
        for (val file : files) {
            size += file.getSize();
        }
        return size;
    }
}
