package ru.cristalix.launcher.download;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import ru.cristalix.launcher.api.data.ClientFile;
import ru.cristalix.launcher.utils.IOUtil;
import ru.cristalix.launcher.utils.LauncherThreads;
import ru.cristalix.launcher.utils.SneakyThrow;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileListVerifier {

    @RequiredArgsConstructor
    @Getter
    public static class VerifyResult {
        private final Collection<ClientFile> requiredToDownloadFiles;
        private final Collection<ClientFile> verifiedFiles;
        private final Collection<ClientFile> classPathFiles;
    }

    public static CompletableFuture<VerifyResult> verifyVerifiedAndGetRequiredAsync(Path clientDirectoryPath,
                                                                                    Collection<Path> currentFiles,
                                                                                    ClientFile[] serverFiles,
                                                                                    String[] verifiedFilesRegexes,
                                                                                    String[] classPathFilesRegexes) {
        return LauncherThreads.supply(() -> {
            val serverFilesMap = Arrays.stream(serverFiles).collect(Collectors.toMap(ClientFile::getPath,
                    clientFile -> clientFile));
            val verifiedRegexSet = Arrays.stream(verifiedFilesRegexes).map(verifiedFile ->
                    Pattern.compile(verifiedFile, Pattern.UNICODE_CHARACTER_CLASS)).collect(Collectors.toList());
            val classPathRegexSet = Arrays.stream(classPathFilesRegexes).map(verifiedFile ->
                    Pattern.compile(verifiedFile, Pattern.UNICODE_CHARACTER_CLASS)).collect(Collectors.toList());
            val clientFiles = new HashSet<String>();
            val requiredFiles = new ArrayList<ClientFile>();
            for (val file : currentFiles) {
                val fileRelativePath = clientDirectoryPath.relativize(file).toString().replace('\\', '/');
                clientFiles.add(fileRelativePath);
                if (verifiedRegexSet.stream().noneMatch(regex -> regex.matcher(fileRelativePath).matches())) {
                    continue;
                }
                if (serverFilesMap.containsKey(fileRelativePath)) {
                    requiredFiles.add(serverFilesMap.get(fileRelativePath));
                    continue;
                }
                clientFiles.remove(fileRelativePath);
                try {
                    IOUtil.deleteIfExists(file);
                } catch (IOException e) {
                    SneakyThrow.sneaky(e);
                }
            }
            for (val file : serverFiles) {
                if (clientFiles.contains(file.getPath())) {
                    continue;
                }
                requiredFiles.add(file);
            }
            val requiredToVerifyFiles = new ArrayList<ClientFile>();
            val classPathFiles = new ArrayList<ClientFile>();
            for (val serverFile : serverFiles) {
                if (verifiedRegexSet.stream().anyMatch(regex -> regex.matcher(serverFile.getPath()).matches())) {
                    requiredToVerifyFiles.add(serverFile);
                }
                if (classPathRegexSet.stream().anyMatch(regex -> regex.matcher(serverFile.getPath()).matches())) {
                    classPathFiles.add(serverFile);
                }
            }
            return new VerifyResult(requiredFiles, requiredToVerifyFiles, classPathFiles);
        });
    }
}
