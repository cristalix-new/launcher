package ru.cristalix.launcher.utils;

import lombok.SneakyThrows;

public class SneakyThrow {

    private SneakyThrow() { }

    @SneakyThrows
    public static RuntimeException sneaky(Throwable t) {
        throw t;
    }
}
