package ru.cristalix.launcher.utils;

import lombok.Value;
import lombok.val;

@Value
public class Version implements Comparable<Version> {

  int major;
  int minor;
  int patch;

  @Override
  public int compareTo(Version o) {
    int r = Integer.compare(major, o.major);
    if (r > 0) {
      return r;
    }
    if ((r = Integer.compare(minor, o.minor)) > 0) {
      return r;
    }
    return Integer.compare(patch, o.patch);
  }

  public static Version parse(String input) {
    val data = input.split("\\.");
    return new Version(parse(data, 0), parse(data, 1), parse(data, 2));
  }

  private static int parse(String[] data, int pos) {
    if (pos >= data.length) {
      throw new IllegalStateException("Expected digit at " + pos);
    }
    return Integer.parseInt(data[pos]);
  }
}
