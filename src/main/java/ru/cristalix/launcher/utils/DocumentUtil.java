package ru.cristalix.launcher.utils;

import lombok.val;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public final class DocumentUtil {

  private static final String[] BROWSERS = new String[]{"google-chrome", "firefox", "opera",
      "konqueror",
      "mozilla"};

  private DocumentUtil() {
  }

  public static void showDocument(String uri) throws IOException {
    val osName = System.getProperty("os.name");
    val rt = Runtime.getRuntime();

    if (osName.startsWith("Mac OS")) {
      Desktop.getDesktop().browse(URI.create(uri));
    } else if (osName.startsWith("Windows")) {
      rt.exec("rundll32 url.dll,FileProtocolHandler " + uri);
    } else {
      val which = new String[] {"which", null};
      for (String b : BROWSERS) {
        which[1] = b;
        if (rt.exec(which).getInputStream().read() != -1) {
          rt.exec(new String[]{b, uri});
          return;
        }
      }

      throw new IllegalStateException("No web browser found");
    }
  }
}
