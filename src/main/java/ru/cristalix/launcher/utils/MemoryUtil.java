package ru.cristalix.launcher.utils;

import sun.misc.Unsafe;

import java.nio.ByteOrder;

public final class MemoryUtil {

  private static final ByteOrder ORDER = ByteOrder.nativeOrder();
  private static final Unsafe UNSAFE = UnsafeUtil.unsafe();

  private MemoryUtil() {
  }

  public static int u_readInt(byte[] src, int srcOff) {
    return UNSAFE.getInt(src, Unsafe.ARRAY_BYTE_BASE_OFFSET + srcOff);
  }

  public static long u_readLong(byte[] src, int srcOff) {
    return UNSAFE.getLong(src, Unsafe.ARRAY_BYTE_BASE_OFFSET + srcOff);
  }

  public static int u_readIntLE(byte[] src, int srcOff) {
    int i = u_readInt(src, srcOff);
    if (ORDER == ByteOrder.BIG_ENDIAN) {
      i = Integer.reverseBytes(i);
    }
    return i;
  }

  public static long u_readLongLE(byte[] src, int srcOff) {
    long i = u_readLong(src, srcOff);
    if (ORDER == ByteOrder.BIG_ENDIAN) {
      i = Long.reverseBytes(i);
    }
    return i;
  }

  public static int readIntBE(byte[] buf, int i) {
    return ((buf[i] & 0xFF) << 24) | ((buf[i+1] & 0xFF) << 16) | ((buf[i+2] & 0xFF) << 8) | (buf[i+3] & 0xFF);
  }

  public static int readIntLE(byte[] buf, int i) {
    return (buf[i] & 0xFF) | ((buf[i+1] & 0xFF) << 8) | ((buf[i+2] & 0xFF) << 16) | ((buf[i+3] & 0xFF) << 24);
  }

  public static int readInt(byte[] buf, int i) {
    if (ORDER == ByteOrder.BIG_ENDIAN) {
      return readIntBE(buf, i);
    } else {
      return readIntLE(buf, i);
    }
  }

  public static long readLongLE(byte[] buf, int i) {
    return (buf[i] & 0xFFL) | ((buf[i+1] & 0xFFL) << 8) | ((buf[i+2] & 0xFFL) << 16) | ((buf[i+3] & 0xFFL) << 24)
        | ((buf[i+4] & 0xFFL) << 32) | ((buf[i+5] & 0xFFL) << 40) | ((buf[i+6] & 0xFFL) << 48) | ((buf[i+7] & 0xFFL) << 56);
  }
}
