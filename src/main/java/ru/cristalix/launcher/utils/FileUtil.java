package ru.cristalix.launcher.utils;

import lombok.val;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public final class FileUtil {

    private static final CopyOption[] COPY_OPTIONS = {StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES};

    public static CompletableFuture<Void> moveDirectoryAsync(Path oldDirectory, Path newDirectory) {
        if (oldDirectory.equals(newDirectory)) {
            return CompletableFuture.completedFuture(null);
        }
        return LauncherThreads.run(() -> {
            try {
                Files.walkFileTree(oldDirectory, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException {
                        val targetPath = newDirectory.resolve(oldDirectory.relativize(dir));
                        IOUtil.createDirectoriesIfNotExists(targetPath);
                        return FileVisitResult.CONTINUE;
                    };

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                        Files.copy(file, newDirectory.resolve(oldDirectory.relativize(file)), COPY_OPTIONS);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                SneakyThrow.sneaky(e);
            }
        }).thenCompose(__ -> removeDirectoryAsync(oldDirectory));
    }

    public static CompletableFuture<Void> removeDirectoryAsync(Path directory) {
        if (!Files.isDirectory(directory)) {
            return CompletableFuture.completedFuture(null);
        }

        return LauncherThreads.run(() -> {
            try {
                Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                        Files.deleteIfExists(file);
                        return super.visitFile(file, attrs);
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                        throws IOException {
                        Files.deleteIfExists(dir);
                        return super.postVisitDirectory(dir, exc);
                    }
                });
            } catch (IOException e) {
                SneakyThrow.sneaky(e);
            }
        });
    }

    public static CompletableFuture<List<Path>> getAllFilesAsync(Path directory) {
        if (!Files.exists(directory)) {
            return CompletableFuture.completedFuture(null);
        }
        return LauncherThreads.supply(() -> {
            try {
                return Files.list(directory).collect(Collectors.toList());
            } catch (IOException ex) {
                throw SneakyThrow.sneaky(ex);
            }
        });
    }
}
