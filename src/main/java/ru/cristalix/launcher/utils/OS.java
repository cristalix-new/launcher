package ru.cristalix.launcher.utils;

import lombok.val;

public final class OS {

  public static final int WINDOWS = 0, LINUX = 1, OSX = 2;

  public static int get() {
    val property = System.getProperty("os.name").toLowerCase();
    if (property.contains("win")) {
      return WINDOWS;
    }
    if (property.contains("linux") || property.contains("nix") || property.contains("nux")) {
      return LINUX;
    }
    if (property.contains("mac")) {
      return OSX;
    }
    throw new IllegalStateException("Unsupported OS: " + property);
  }
}
