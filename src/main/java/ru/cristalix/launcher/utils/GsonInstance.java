package ru.cristalix.launcher.utils;

import com.google.gson.Gson;

public final class GsonInstance {

  public static final Gson GSON = new Gson();

  private GsonInstance() { }
}
