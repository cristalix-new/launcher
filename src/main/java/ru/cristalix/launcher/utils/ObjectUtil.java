package ru.cristalix.launcher.utils;

public final class ObjectUtil {

  private ObjectUtil() { }

  public static <T> T firstNotNull(T nullable, T o) {
    return nullable != null ? nullable : o;
  }
}
