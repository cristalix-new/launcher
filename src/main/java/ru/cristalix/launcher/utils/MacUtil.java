package ru.cristalix.launcher.utils;

import lombok.val;
import ru.cristalix.launcher.Launcher;

import javax.imageio.ImageIO;
import java.awt.*;

public final class MacUtil {

  public static final boolean FUCK_MAC = System.getProperty("os.name").toLowerCase().contains("mac");

  private MacUtil() {
  }

  public static void setup() {
    if (FUCK_MAC) {
      try {
        val c = Class.forName("com.apple.eawt._AppDockIconHandler");
        val constr = c.getDeclaredConstructor();
        constr.setAccessible(true);
        val instance = constr.newInstance();
        val image = ImageIO.read(Launcher.class.getResourceAsStream("/images/icons/icon.png"));
        val m = c.getDeclaredMethod("setDockIconImage", Image.class);
        m.setAccessible(true);
        m.invoke(instance, image);
      } catch (Exception e) {
        System.err.println("Could not change dock icon: ");
        e.printStackTrace();
      }
    }
  }
}
