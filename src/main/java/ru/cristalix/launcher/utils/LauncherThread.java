package ru.cristalix.launcher.utils;

import lombok.val;
import lombok.var;
import ru.cristalix.launcher.xxhash.StreamingXXHash64;
import ru.cristalix.launcher.xxhash.StreamingXXHash64JavaSafe;
import ru.cristalix.launcher.xxhash.StreamingXXHash64JavaUnsafe;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class LauncherThread extends Thread {

  private byte[] byteBuffer;
  private char[] textBuffer;
  private byte[] longBuffer;
  private byte[] hexBuffer;
  private ReusableBufferedOutputStream writeBuffer;
  private StreamingXXHash64 xxHash64;
  private MessageDigest sha256;

  public LauncherThread(Runnable target, String name) {
    super(target, name);
  }

  public StreamingXXHash64 xxHash64() {
    StreamingXXHash64 xxHash64 = this.xxHash64;
    if (xxHash64 == null) {
      return this.xxHash64 = (UnsafeUtil.isUnalignedAccessAllowed()
          ? new StreamingXXHash64JavaUnsafe(0L) : new StreamingXXHash64JavaSafe(0L));
    }
    xxHash64.reset();
    return xxHash64;
  }

  public MessageDigest sha256() {
    MessageDigest sha256 = this.sha256;
    if (sha256 == null) {
      try {
        return this.sha256 = MessageDigest.getInstance("SHA-256");
      } catch (NoSuchAlgorithmException ex) {
        throw SneakyThrow.sneaky(ex);
      }
    }
    sha256.reset();
    return sha256;
  }

  public byte[] longBuffer() {
    var buf = this.longBuffer;
    if (buf == null) {
      return this.longBuffer = new byte[8];
    }
    return buf;
  }

  public byte[] hexBuffer(int length) {
    var buffer = this.hexBuffer;
    if (buffer == null || buffer.length < length) {
      return this.hexBuffer = new byte[length];
    }
    return buffer;
  }

  public byte[] byteBuffer() {
    val buffer = this.byteBuffer;
    if (buffer == null) {
      return this.byteBuffer = new byte[32768];
    }
    return buffer;
  }

  public char[] textBuffer() {
    val buffer = this.textBuffer;
    if (buffer == null) {
      return this.textBuffer = new char[32768];
    }
    return buffer;
  }

  public ReusableBufferedOutputStream writeBuffer() {
    val writeBuffer = this.writeBuffer;
    if (writeBuffer == null) {
      return this.writeBuffer = new ReusableBufferedOutputStream();
    }
    return writeBuffer;
  }
}
