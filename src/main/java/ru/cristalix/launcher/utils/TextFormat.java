package ru.cristalix.launcher.utils;

public final class TextFormat {

  private TextFormat() {
  }

  public static String clientDelete(String client) {
    return "Удаление файлов клиента " + client;
  }

  public static String jsonPath(String path) {
    return path + ".json";
  }

  public static String xmx(long memoryAmount) {
    return "-Xmx" + memoryAmount + "M";
  }

  public static String backgroundPath(int idx) {
    return "/images/backgrounds/" + idx + ".jpg";
  }

  public static String backgroundPreviewPath(int idx) {
    return "/images/backgrounds/previews/" + idx + ".png";
  }

  public static String favourite(String id) {
    return "isFavourite-" + id;
  }

  public static String serverIcon(String id) {
    return "/images/icons/servers/" + id + ".png";
  }

  public static String serverBackground(String id) {
    return "/images/backgrounds/servers/" + id + ".png";
  }

  public static String ram(long memory) {
    return "RAM для игры: " + (memory < 512L ? "автоматически" : (memory + " МБ"));
  }
}
