package ru.cristalix.launcher.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class CompletableFutureUtil {

    @Setter
    @Getter
    public static final class Combined<T1, T2> {
        private T1 t1;
        private T2 t2;
    }

    public static <T1, T2> CompletableFuture<Combined<T1, T2>> combine(CompletableFuture<T1> t1CompletableFuture,
                                                                       CompletableFuture<T2> t2CompletableFuture) {
        val result = new Combined<T1, T2>();

        return CompletableFuture.allOf(t1CompletableFuture.thenAccept(result::setT1),
                t2CompletableFuture.thenAccept(result::setT2)).thenApply(_u -> result);
    }

    public static <T1, T2> CompletableFuture<Combined<T1, T2>> combine(Executor executor, CompletableFuture<T1> t1CompletableFuture,
                                                                       CompletableFuture<T2> t2CompletableFuture) {
        val result = new Combined<T1, T2>();

        return CompletableFuture.allOf(t1CompletableFuture.thenAccept(result::setT1),
                t2CompletableFuture.thenAccept(result::setT2)).thenApplyAsync(_u -> result, executor);
    }

    public static <T> CompletableFuture<T> excepted(Throwable throwable) {
        val result = new CompletableFuture<T>();
        result.completeExceptionally(throwable);
        return result;
    }
}
