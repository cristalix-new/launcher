package ru.cristalix.launcher.utils;

import lombok.val;
import sun.misc.Unsafe;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Field;
import java.nio.ByteOrder;
import java.util.Objects;

public final class UnsafeUtil {

  private static final ByteOrder ORDER = ByteOrder.nativeOrder();
  private static final boolean UNALIGNED_ACCESS_ALLOWED;
  private static final Unsafe UNSAFE;
  private static final Lookup LOOKUP;

  private UnsafeUtil() {
  }

  public static Unsafe unsafe() {
    return UNSAFE;
  }

  public static Lookup lookup() {
    return LOOKUP;
  }

  public static boolean isUnalignedAccessAllowed() {
    return UNALIGNED_ACCESS_ALLOWED;
  }

  public static void putLong(long v, byte[] buf) {
    UNSAFE.putLong(buf, Unsafe.ARRAY_BYTE_BASE_OFFSET, (ORDER == ByteOrder.LITTLE_ENDIAN) ? Long.reverseBytes(v) : v);
  }

  static {
    try {
      Field field = Unsafe.class.getDeclaredField("theUnsafe");
      field.setAccessible(true);
      val unsafe = UNSAFE = (Unsafe) field.get(null);
      unsafe.ensureClassInitialized(MethodHandles.class);
      unsafe.ensureClassInitialized(Lookup.class);
      field = Lookup.class.getDeclaredField("IMPL_LOOKUP");
      field.setAccessible(true);
      LOOKUP = (Lookup) Objects.requireNonNull(
          unsafe.getObject(unsafe.staticFieldBase(field), unsafe.staticFieldOffset(field)),
          "IMPL_LOOKUP");
    } catch (IllegalAccessException | NoSuchFieldException ex) {
      throw new ExceptionInInitializerError(ex);
    }
    val arch = System.getProperty("os.arch");
    UNALIGNED_ACCESS_ALLOWED = arch.equals("i386") || arch.equals("x86")
        || arch.equals("amd64") || arch.equals("x86_64")
        || arch.equals("aarch64") || arch.equals("ppc64le");
  }
}
