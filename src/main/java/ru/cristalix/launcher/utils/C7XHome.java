package ru.cristalix.launcher.utils;

import lombok.val;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class C7XHome {

    private static final Path HOME_DIR;

    private C7XHome() {
    }

    public static Path resolve(Path path) {
        return HOME_DIR.resolve(path);
    }

    public static Path resolve(String path) {
        return HOME_DIR.resolve(path);
    }

    public static void createHomeDir() throws IOException {
        IOUtil.createDirectoriesIfNotExists(HOME_DIR);
    }

    private static Path detectHomeDir() {
        val userHome = System.getProperty("user.home", ".");
        switch (OS.get()) {
            case OS.LINUX:
                return Paths.get(userHome, ".cristalix");
            case OS.WINDOWS: {
                val applicationData = System.getenv("APPDATA");
                val folder = applicationData != null ? applicationData : userHome;
                return Paths.get(folder, ".cristalix");
            }
            case OS.OSX:
                return Paths.get(userHome, "Library/Application Support/cristalix");
            default:
                return Paths.get(userHome, "cristalix");
        }
    }

    static {
        HOME_DIR = detectHomeDir();
    }
}
