package ru.cristalix.launcher.utils;

import java.util.concurrent.*;
import java.util.function.Supplier;

public final class LauncherThreads {

    public static final ExecutorService THREAD_POOL = new ThreadPoolExecutor(
            0, Integer.MAX_VALUE,
            7L, TimeUnit.SECONDS,
            new SynchronousQueue<>(),
            new ThreadFactoryBuilder().setNameFormat("Launcher Thread #").build());

    private LauncherThreads() {
    }

    public static <T> CompletableFuture<T> supply(Supplier<T> supplier) {
        return CompletableFuture.supplyAsync(supplier, THREAD_POOL);
    }

    public static CompletableFuture<Void> run(Runnable command) {
        return CompletableFuture.runAsync(command, THREAD_POOL);
    }

    public static <T1, T2> CompletableFuture<CompletableFutureUtil.Combined<T1, T2>> combine(CompletableFuture<T1> t1CompletableFuture,
                                                                                             CompletableFuture<T2> t2CompletableFuture) {
        return CompletableFutureUtil.combine(THREAD_POOL, t1CompletableFuture, t2CompletableFuture);
    }
}
