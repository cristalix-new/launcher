package ru.cristalix.launcher.utils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class ReusableBufferedOutputStream extends BufferedOutputStream {

  public ReusableBufferedOutputStream() {
    super(null);
  }

  public void reset(OutputStream os) {
    out = os;
    count = 0;
  }

  @Override
  public void close() throws IOException {
    try {
      super.close();
    } finally {
      count = 0;
      out = null;
    }
  }
}
