package ru.cristalix.launcher.utils;

import lombok.SneakyThrows;
import lombok.val;

import java.io.*;
import java.lang.invoke.MethodHandle;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.file.*;

public final class IOUtil {

  private static final LinkOption[] LINK = {};
  private static final OpenOption[] READ = {};
  private static final OpenOption[] WRITE = {StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING};
  private static final MethodHandle SET_BR_BUFFER;
  private static final MethodHandle SET_BW_BUFFER;

  private IOUtil() {
  }

  private static HttpURLConnection init(URL url) throws IOException {
    val con = (HttpURLConnection) url.openConnection(Proxy.NO_PROXY);
    con.setConnectTimeout(3000);
    con.setDoInput(true);
    con.setRequestProperty("User-Agent", "Launcher/1.0");
    return con;
  }

  public static HttpURLConnection get(URL url) throws IOException {
    return init(url);
  }

  public static HttpURLConnection get(String url) throws IOException {
    return get(new URL(url));
  }

  public static HttpURLConnection post(URL url, byte[] data, String contentType)
      throws IOException {
    val connection = init(url);
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", contentType);
    connection.setDoOutput(true);
    try (val os = connection.getOutputStream()) {
      os.write(data);
    }
    return connection;
  }

  public static HttpURLConnection post(String url, byte[] data, String contentType)
      throws IOException {
    return post(new URL(url), data, contentType);
  }

  public static BufferedReader reader(Path path, Charset charset) throws IOException {
    val isr = new InputStreamReader(Files.newInputStream(path) ,charset);
    val br = new BufferedReader(isr, 1);
    setReaderBuffer(br, textBuffer());
    return br;
  }

  public static BufferedReader reader(InputStream in, Charset charset) {
    val isr = new InputStreamReader(in ,charset);
    val br = new BufferedReader(isr, 1);
    setReaderBuffer(br, textBuffer());
    return br;
  }

  public static BufferedWriter writer(Path path, Charset charset) throws IOException {
    val bw = new BufferedWriter(new OutputStreamWriter(newOutputStream(path), charset.newEncoder()), 1);
    setWriterBuffer(bw, textBuffer());
    return bw;
  }

  public static void closeQuietly(Closeable c) {
    try {
      c.close();
    } catch (IOException ignored) {
    }
  }

  public static InputStream newInputStream(Path path) throws IOException {
    return Files.newInputStream(path, READ);
  }

  public static OutputStream newOutputStream(Path path) throws IOException {
    val os = writeBuffer();
    os.reset(Files.newOutputStream(path, WRITE));
    return os;
  }

  public static boolean isFile(Path path) {
    return path.toFile().isFile();
  }

  public static long size(Path path) {
    return path.toFile().length();
  }

  public static void deleteIfExists(Path path) throws IOException {
    val file = path.toFile();
    if (file.isFile() && !file.delete()) {
      throw new IOException("Failed to delete: " + path);
    }
  }

  public static byte[] byteBuffer() {
    val th = Thread.currentThread();
    if (th instanceof LauncherThread) {
      return ((LauncherThread) th).byteBuffer();
    }
    return ThreadLocals.byteBuffer();
  }

  public static char[] textBuffer() {
    val th = Thread.currentThread();
    if (th instanceof LauncherThread) {
      return ((LauncherThread) th).textBuffer();
    }
    return ThreadLocals.textBuffer();
  }

  private static ReusableBufferedOutputStream writeBuffer() {
    val th = Thread.currentThread();
    if (th instanceof LauncherThread) {
      return ((LauncherThread) th).writeBuffer();
    }
    return ThreadLocals.writeBuffer();
  }

  public static void createDirectoriesIfNotExists(Path path) throws IOException {
    val file = path.toFile();
    if (!file.mkdirs() && !file.isDirectory()) {
      throw new IOException("Could not create directory tree: " + path);
    }
  }

  public static void createDirectoryIfNotExists(Path path) throws IOException {
    val file = path.toFile();
    if (!file.mkdir() && !file.isDirectory()) {
      throw new IOException("Could not create directory: " + path);
    }
  }

  public static boolean isRegularFile(Path path) {
    return Files.isRegularFile(path, LINK);
  }

  public static void setSocketFlags(Socket sock) {
    try {
      sock.setKeepAlive(false);
      sock.setTcpNoDelay(true);
      sock.setReuseAddress(true);
    } catch(Exception ignored) {
    }
    try {
      sock.setTrafficClass(0b11100);
    } catch(Exception ignored) {
    }
    try {
      sock.setPerformancePreferences(1, 0, 2);
    } catch(Exception ignored) {
    }
  }

  public static void setSocketFlags(ServerSocket sock) {
    try {
      sock.setPerformancePreferences(1, 0, 2);
    } catch(Exception ignored) {
    }
  }

  @SneakyThrows
  private static void setReaderBuffer(BufferedReader reader, char[] buffer) {
    SET_BR_BUFFER.invokeExact(reader, buffer);
  }

  @SneakyThrows
  private static void setWriterBuffer(BufferedWriter writer, char[] buffer) {
    SET_BW_BUFFER.invokeExact(writer, buffer);
  }

  static {
    try {
      val lookup = UnsafeUtil.lookup();
      SET_BR_BUFFER = lookup.findSetter(BufferedReader.class, "cb", char[].class);
      SET_BW_BUFFER = lookup.findSetter(BufferedWriter.class, "cb", char[].class);
    } catch (NoSuchFieldException | IllegalAccessException ex) {
      throw new ExceptionInInitializerError(ex);
    }
  }
}
