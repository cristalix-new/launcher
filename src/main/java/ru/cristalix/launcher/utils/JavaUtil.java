package ru.cristalix.launcher.utils;

import lombok.SneakyThrows;
import lombok.val;
import sun.reflect.ReflectionFactory;

import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JavaUtil {

    @SneakyThrows(URISyntaxException.class)
    public static Path getLauncherPath() {
        return Paths.get(JavaUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI());
    }

    public static Path getJavaPath() {
        return Paths.get(System.getProperty("java.home"), "bin", "java");
    }

    public static long getTotalMemory() {
        return Math.min(8192, Runtime.getRuntime().totalMemory());
    }

    public static void longToBytes(long x, byte[] result) {
        UnsafeUtil.putLong(x, result);
    }

    @SneakyThrows
    public static void addPath(ClassLoader loader, Path path) {
        val m = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
        m.setAccessible(true);
        ReflectionFactory.getReflectionFactory()
                .newMethodAccessor(m)
                .invoke(loader, new Object[]{path.toUri().toURL()});
    }
}
