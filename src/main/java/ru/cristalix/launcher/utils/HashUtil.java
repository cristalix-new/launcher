package ru.cristalix.launcher.utils;

import dev.xdark.asminline.AsmBlock;
import lombok.val;
import lombok.var;
import ru.cristalix.launcher.xxhash.StreamingXXHash64;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.concurrent.CompletableFuture;

public final class HashUtil {

    private static final CompletableFuture<String> NONE = CompletableFuture.completedFuture(null);
    private static final byte[] HEX_ARRAY = AsmBlock.inlineA(b -> b.array("0123456789abcdef".getBytes(StandardCharsets.US_ASCII)));

    private static byte[] hexBuffer(int j) {
        val th = Thread.currentThread();
        return th instanceof LauncherThread ? ((LauncherThread) th).hexBuffer(j) : new byte[j];
    }

    private static String bytesToHex(byte[] bytes) {
        val j = bytes.length;
        val l = j * 2;
        val hexChars = hexBuffer(l);
        val HEX_ARRAY = HashUtil.HEX_ARRAY;
        for (int i = 0; i < j; i++) {
            val v = bytes[i] & 0xFF;
            val k = i * 2;
            hexChars[k] = HEX_ARRAY[v >>> 4];
            hexChars[k + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, 0, l, StandardCharsets.UTF_8);
    }

    private static byte[] longToBytes(long x) {
        val th = Thread.currentThread();
        val tmp = th instanceof LauncherThread ? ((LauncherThread) th).longBuffer() : new byte[8];
        JavaUtil.longToBytes(x, tmp);
        return tmp;
    }

    public static CompletableFuture<String> getXxHash(Path file) {
        if (!IOUtil.isRegularFile(file)) {
            return NONE;
        }
        return LauncherThreads.supply(() -> {
            if (!IOUtil.isRegularFile(file)) {
                return null;
            }

            try {
                val hash = newStreamingHash64();
                val buffer = IOUtil.byteBuffer();
                try (val fileInputStream = IOUtil.newInputStream(file)) {
                    var read = 0;
                    while ((read = fileInputStream.read(buffer)) > 0) {
                        hash.update(buffer, 0, read);
                    }
                }

                return bytesToHex(longToBytes(hash.getValue()));
            } catch (Exception e) {
                throw SneakyThrow.sneaky(e);
            }
        });
    }

    public static CompletableFuture<String> getSha256Hash(Path file) {
        if (!IOUtil.isRegularFile(file)) {
            return NONE;
        }
        return LauncherThreads.supply(() -> {
            if (!IOUtil.isRegularFile(file)) {
                return null;
            }

            try {
                val hash = sha256();
                val buffer = IOUtil.byteBuffer();
                try (val fileInputStream = IOUtil.newInputStream(file)) {
                    var read = 0;
                    while ((read = fileInputStream.read(buffer)) > 0) {
                        hash.update(buffer, 0, read);
                    }
                }

                return bytesToHex(hash.digest());
            } catch (Exception e) {
                throw SneakyThrow.sneaky(e);
            }
        });
    }

    private static StreamingXXHash64 newStreamingHash64() {
        val th = Thread.currentThread();
        return th instanceof LauncherThread ? ((LauncherThread) th).xxHash64() : ThreadLocals.xxHash64();
    }

    private static MessageDigest sha256() {
        val th = Thread.currentThread();
        if (th instanceof LauncherThread) {
            return ((LauncherThread) th).sha256();
        }
        return ThreadLocals.sha256();
    }
}
