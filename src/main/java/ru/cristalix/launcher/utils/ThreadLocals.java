package ru.cristalix.launcher.utils;

import lombok.val;
import ru.cristalix.launcher.xxhash.StreamingXXHash64;
import ru.cristalix.launcher.xxhash.StreamingXXHash64JavaSafe;
import ru.cristalix.launcher.xxhash.StreamingXXHash64JavaUnsafe;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class ThreadLocals {

  private static final ThreadLocal<byte[]> BYTES = ThreadLocal.withInitial(() -> new byte[32768]);
  private static final ThreadLocal<char[]> CHARS = ThreadLocal.withInitial(() -> new char[32768]);
  private static final ThreadLocal<ReusableBufferedOutputStream> WRITE_BUFFER = ThreadLocal.withInitial(ReusableBufferedOutputStream::new);
  private static final ThreadLocal<StreamingXXHash64> XX_HASH_64 = ThreadLocal.withInitial(UnsafeUtil.isUnalignedAccessAllowed() ? () -> new StreamingXXHash64JavaUnsafe(0L) : () -> new StreamingXXHash64JavaSafe(0L));
  private static final ThreadLocal<MessageDigest> SHA_256 = ThreadLocal.withInitial(() -> {
    try {
      return MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException ex) {
      throw SneakyThrow.sneaky(ex);
    }
  });

  private ThreadLocals() { }

  public static byte[] byteBuffer() {
    return BYTES.get();
  }

  public static char[] textBuffer() {
    return CHARS.get();
  }

  public static ReusableBufferedOutputStream writeBuffer() {
    return WRITE_BUFFER.get();
  }

  public static StreamingXXHash64 xxHash64() {
    val xxHash64 = XX_HASH_64.get();
    xxHash64.reset();
    return xxHash64;
  }

  public static MessageDigest sha256() {
    val digest = SHA_256.get();
    digest.reset();
    return digest;
  }
}
