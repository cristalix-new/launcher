package ru.cristalix.launcher.utils;

import lombok.val;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class ThreadFactoryBuilder {
  private String nameFormat;

  public ThreadFactoryBuilder setNameFormat(String nameFormat) {
    this.nameFormat = nameFormat;
    return this;
  }

  public ThreadFactory build() {
    val nameFormat = this.nameFormat;
    val count = new AtomicLong(0);
    return new ThreadFactory() {
      @Override
      public Thread newThread(Runnable runnable) {
        return new LauncherThread(runnable, nameFormat + count.getAndIncrement());
      }
    };
  }

}
