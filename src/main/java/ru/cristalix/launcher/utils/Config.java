package ru.cristalix.launcher.utils;

import lombok.val;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Properties;

public class Config {
    private final Properties config = new Properties();

    public synchronized String get(String key) {
        return config.getProperty(key);
    }

    public synchronized String getOrDefault(String key, String defaultValue) {
        return config.getProperty(key, defaultValue);
    }

    public synchronized long getLong(String key) {
        return Long.parseLong(config.getProperty(key));
    }

    public synchronized long getLong(String key, long defaultValue) {
        val value = config.getProperty(key);
        return value != null ? Long.parseLong(value) : defaultValue;
    }

    public synchronized void set(String key, String value) {
        if (value == null) {
            config.remove(key);
            return;
        }
        config.put(key, value);
    }

    public boolean getBoolean(String key) {
        return "1".equals(get(key));
    }

    public void setBoolean(String key, boolean value) {
        set(key, value ? "1" : "0");
    }

    public synchronized void save(Path filePath) throws IOException {
        try (val configOutputStream = IOUtil.writer(filePath, StandardCharsets.UTF_8)) {
            config.store(configOutputStream, null);
        }
    }

    public static Config load(Path filePath) throws IOException {
        val config = new Config();
        try (val configInputStream = IOUtil.reader(filePath, StandardCharsets.UTF_8)) {
            config.config.load(configInputStream);
        }
        return config;
    }
}
