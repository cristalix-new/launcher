package ru.cristalix.launcher.relaunch;

import lombok.val;
import ru.cristalix.launcher.utils.JavaUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;

public final class Relauncher {

  public static void relaunch(Path launcherPath, String[] args) throws IOException {
    val command = new LinkedList<String>();
    command.add(JavaUtil.getJavaPath().toString());
    command.add("-jar");
    command.add(launcherPath.toString());
    Collections.addAll(command, args);
    new ProcessBuilder().command(command).start();
  }
}
