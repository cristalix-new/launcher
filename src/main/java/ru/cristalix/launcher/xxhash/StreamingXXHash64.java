package ru.cristalix.launcher.xxhash;

public abstract class StreamingXXHash64 {

  final long seed;

  StreamingXXHash64(long seed) {
    this.seed = seed;
  }

  public abstract long getValue();

  public abstract void update(byte[] buf, int off, int len);

  public abstract void reset();

  @Override
  public String toString() {
    return getClass().getSimpleName() + "(seed=" + seed + ")";
  }
}
