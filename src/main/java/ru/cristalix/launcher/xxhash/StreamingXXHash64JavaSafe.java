package ru.cristalix.launcher.xxhash;

import ru.cristalix.launcher.utils.MemoryUtil;

public final class StreamingXXHash64JavaSafe extends AbstractStreamingXXHash64Java {

  public StreamingXXHash64JavaSafe(long seed) {
    super(seed);
  }

  @Override
  protected long readLongLE(byte[] memory, int off) {
    return MemoryUtil.readLongLE(memory, off);
  }

  @Override
  protected int readIntLE(byte[] memory, int off) {
    return MemoryUtil.readIntLE(memory, off);
  }
}