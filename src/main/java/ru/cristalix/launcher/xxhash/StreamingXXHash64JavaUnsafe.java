package ru.cristalix.launcher.xxhash;

import ru.cristalix.launcher.utils.MemoryUtil;

public final class StreamingXXHash64JavaUnsafe extends AbstractStreamingXXHash64Java {

  public StreamingXXHash64JavaUnsafe(long seed) {
    super(seed);
  }

  @Override
  protected long readLongLE(byte[] memory, int off) {
    return MemoryUtil.u_readLongLE(memory, off);
  }

  @Override
  protected int readIntLE(byte[] memory, int off) {
    return MemoryUtil.u_readIntLE(memory, off);
  }
}