package ru.cristalix.launcher;

import ru.cristalix.launcher.api.LaunchServerApi;

public final class LaunchServerApiInstance {

  public static final LaunchServerApi API = new LaunchServerApi("https://launcher-new.c7x.dev/api");

  private LaunchServerApiInstance() { }
}
