package ru.cristalix.launcher;

import com.sun.glass.ui.Application;
import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import ru.cristalix.launcher.api.ExceptionAlert;
import ru.cristalix.launcher.api.LaunchServerApi;
import ru.cristalix.launcher.download.Downloader;
import ru.cristalix.launcher.gui.FXApplication;
import ru.cristalix.launcher.utils.*;

import javax.imageio.ImageIO;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.PlatformLoggingMXBean;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public final class Launcher implements Runnable {

    private final String[] args;
    @Getter
    private final LaunchServerApi api = LaunchServerApiInstance.API;
    @Getter
    private Config config;
    @Getter
    private final Path configPath = C7XHome.resolve(".launcher");

    public static void main(String[] args) {
        val delay = Long.getLong("launcher.startupDelay");
        if (delay != null && delay > 0L) {
            System.err.printf("Sleeping for %dms%n", delay);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ignored) {
            }
        }
        new LauncherThread(new Launcher(args), "Startup Thread").start();
    }

    @Override
    public void run() {
        System.out.println("Starting");
        val keeper = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(Long.MAX_VALUE);
                } catch (InterruptedException ignored) {
                    break;
                }
            }
        }, "Infinite sleeper");
        keeper.setDaemon(false);
        keeper.setPriority(Thread.MIN_PRIORITY);
        keeper.start();
        val args = this.args;
        if (args.length > 0) {
            val arg = args[0];
            if ("verify".equals(arg)) {
                if (args.length < 2) {
                    return;
                }
                String myHash;
                try {
                    myHash = HashUtil.getSha256Hash(JavaUtil.getLauncherPath()).get(15L, TimeUnit.SECONDS);
                } catch (Exception e) {
                    ExceptionAlert.show(e, "Обновление лаунчера");
                    return;
                }
                if (!args[1].equals(myHash)) {
                    Runtime.getRuntime().halt(1);
                    return;
                }
            }
        }
        System.out.println("Loading config");
        try {
            C7XHome.createHomeDir();
            loadConfig();
        } catch (Exception e) {
            ExceptionAlert.show(e, "Загрузка конфигурации");
            return;
        }
        System.out.println("Initializing JavaFX");
        initializeJfx();
    }

    private void initializeJfx() {
        System.setProperty("prism.lcdtext", "false");
        System.setProperty("jdk.gtk.version", "2");
        System.setProperty("javafx.autoproxy.disable", "true");
        System.setProperty("glass.disableThreadChecks", "true");
        System.setProperty("prism.vsync", "false");
        System.setProperty("prism.cacheshapes", "all");
        ImageIO.setUseCache(false);

        System.out.println("Loading assets");
        val assetsPath = C7XHome.resolve("assets.jar");
        LauncherThreads.combine(HashUtil.getSha256Hash(assetsPath), api.getAssets())
                .thenCompose(result -> {
                    val response = result.getT2();
                    if (!response.getHash().equals(result.getT1())) {
                        System.out.println("Assets hash mismatch, re-downloading");
                        return Downloader.download(response.getUrl(), assetsPath, bytes -> {
                        });
                    }
                    System.out.println("Assets matched");
                    return CompletableFuture.completedFuture(null);
                })
                .thenRun(() -> {
                    System.out.println("Initializing UI");
                    JavaUtil.addPath(Launcher.class.getClassLoader(), assetsPath);
                    PlatformImpl.startup(() -> {
                        Application.GetApplication().setName("Cristalix");
                        MacUtil.setup();
                        try {
                            val field = Class.forName("com.sun.javafx.tk.quantum.PrismImageLoader2$AsyncImageLoader", true, Launcher.class.getClassLoader())
                                    .getDeclaredField("BG_LOADING_EXECUTOR");
                            val unsafe = UnsafeUtil.unsafe();
                            val base = unsafe.staticFieldBase(field);
                            val offset = unsafe.staticFieldOffset(field);
                            ((ExecutorService) unsafe.getObject(base, offset)).shutdown();
                            unsafe.putObject(base, offset, LauncherThreads.THREAD_POOL);
                        } catch (Exception ex) {
                            System.err.println("Could not replace image loader thread pool: ");
                            ex.printStackTrace();
                        }
                    });
                    Platform.runLater(() -> {
                        try {
                            val stage = new Stage();
                            stage.impl_setPrimary(true);
                            FXApplication.start(this, stage);
                            try {
                                ManagementFactory.getPlatformMXBean(PlatformLoggingMXBean.class)
                                        .setLoggerLevel("javafx.css", "OFF");
                            } catch (IllegalArgumentException ignored) {
                            }
                        } catch (Throwable e) {
                            ExceptionAlert.show(e, "Загрузка интерфейса");
                        }
                    });
                })
                .exceptionally(t -> {
                    System.err.println("Could not load resources: ");
                    t.printStackTrace();
                    ExceptionAlert.show(t, "Загрузка ресурсов");
                    return null;
                });
    }

    private void loadConfig() throws IOException {
        if (!IOUtil.isFile(configPath)) {
            config = new Config();
        } else {
            config = Config.load(configPath);
        }
    }


    @SneakyThrows
    private static void replaceSocksFactory() {
        val unsafe = UnsafeUtil.unsafe();
        {
            val factory = SocketFactory.getDefault();
            val field = SocketFactory.class.getDeclaredField("theFactory");
            unsafe.putObject(unsafe.staticFieldBase(field), unsafe.staticFieldOffset(field), new SocketFactory() {
                @Override
                public Socket createSocket() throws IOException {
                    val sock = super.createSocket();
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
                    val sock = factory.createSocket(host, port);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
                        throws IOException, UnknownHostException {
                    val sock = factory.createSocket(host, port, localHost, localPort);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(InetAddress host, int port) throws IOException {
                    val sock = factory.createSocket(host, port);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(InetAddress address, int port, InetAddress localAddress,
                                           int localPort) throws IOException {
                    val sock = factory.createSocket(address, port, localAddress, localPort);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }
            });
        }
        {
            val factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            val field = SSLSocketFactory.class.getDeclaredField("theFactory");
            unsafe.putObject(unsafe.staticFieldBase(field), unsafe.staticFieldOffset(field), new SSLSocketFactory() {

                @Override
                public String[] getDefaultCipherSuites() {
                    return factory.getDefaultCipherSuites();
                }

                @Override
                public String[] getSupportedCipherSuites() {
                    return factory.getSupportedCipherSuites();
                }

                @Override
                public Socket createSocket() throws IOException {
                    val sock = factory.createSocket();
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(Socket s, InputStream consumed, boolean autoClose)
                        throws IOException {
                    val sock = factory.createSocket(s, consumed, autoClose);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(Socket s, String host, int port, boolean autoClose)
                        throws IOException {
                    val sock = factory.createSocket(s, host, port, autoClose);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
                    val sock = factory.createSocket(host, port);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(String host, int port, InetAddress localHost, int localPort)
                        throws IOException {
                    val sock = factory.createSocket(host, port, localHost, localPort);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(InetAddress host, int port) throws IOException {
                    val sock = factory.createSocket(host, port);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }

                @Override
                public Socket createSocket(InetAddress address, int port, InetAddress localAddress,
                                           int localPort) throws IOException {
                    val sock = factory.createSocket(address, port, localAddress, localPort);
                    IOUtil.setSocketFlags(sock);
                    return sock;
                }
            });
        }
    }

    static {
        System.setProperty("apple.awt.application.name", "Cristalix");
        System.setProperty("jdk.nio.enableFastFileTransfer", "true");
        System.setProperty("jdk.net.useFastTcpLoopback", "true");
        System.setProperty("java.net.preferIPv4Stack", "true");
        System.setProperty("http.maxConnections", "10");
        System.setProperty("https.agent", "Launcher/1.0");
        // Setup SSL sock. factory
        replaceSocksFactory();
    }
}
